package config

import (
	"errors"
	"fmt"
	"os"
	"os/user"

	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/fs"
)

const (
	defaultPath = "~/.config/lockbox/"

	// defaultConfigFile residing in the users home directory
	defaultConfigFile = "lockbox.toml"
)

func Init(cfgFile string) error {
	if cfgFile == "" {
		var err error
		cfgFile, err = fs.ExpandHome(defaultPath + defaultConfigFile)
		if err != nil {
			logrus.WithError(err).Error("unable to expand to users home dir")
			return err
		}
	}

	created := false
	err := fs.FileExists(cfgFile)
	switch {
	case errors.Is(err, fs.ErrFileNotExists):
		if _, err := fs.CreateFileWithPath(cfgFile, 0700); err != nil {
			logrus.WithError(err).Error("unable to create empty config file")
			return err
		}
		created = true

	case err != nil:
		logrus.WithError(err).Panic("checking for config file failed")
	}

	if err := ReadConfig(cfgFile); err != nil {
		return err
	}

	if created {
		// config was created, set some sensible defaults
		err = setDefaults()
		if err != nil {
			logrus.WithError(err).Error("failed setting default config values")
			return errors.New("unable to set default config values")
		}

		if err := UpdateConfig(); err != nil {
			logrus.WithError(err).Warning("unable to write config file")
			return nil
		}
	}

	if err := checkLockboxPaths(); err != nil {
		return err
	}

	return nil
}

// checkLockboxPaths checks for existance and writablity of the general path and
// the password paths. If the paths do not exist it creates them.
func checkLockboxPaths() error {
	for _, path := range []string{
		"general.path",
		"general.password_path",
	} {
		if err := checkPathWritability(GetString(path)); err != nil {
			return err
		}
	}

	return nil
}

// checkPathWritability checks if the path exists, then creates it or checks
// writability of an existing path.
func checkPathWritability(path string) (err error) {
	path, err = fs.ExpandHome(path)
	if err != nil {
		return err
	}

	if _, err = os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0700)
		if err != nil {
			logrus.WithError(err).
				WithField("path", path).
				Error("unable to create")
			return
		}

		return
	}

	return fs.IsDirWritable(path)
}

// setDefaults set default config values for lockbox.
func setDefaults() error {
	u, err := user.Current()
	if err != nil {
		return err
	}

	Set("general.sockfile", fmt.Sprintf("/var/run/user/%s/lockbox.sock", u.Uid))
	Set("general.path", defaultPath)
	Set("general.password_path", defaultPath+"data/")
	Set("general.key_path", defaultPath+"keys/")

	Set("daemon.logfile", defaultPath+"lockboxd.log")

	Set("client.logfile", defaultPath+"lockbox.log")

	Set("password.length", 25)
	Set("password.nosymbols", false)

	return nil
}
