package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/slax0rr/lockbox/internal/fs"
)

// InitConfig reads in the config file or creates it if it does not exist.
func ReadConfig(cfgFile string) error {
	viper.SetConfigFile(cfgFile)
	viper.AutomaticEnv()

	logrus.WithField("cfg_file", cfgFile).Debug("reading config")

	if err := viper.ReadInConfig(); err != nil {
		logrus.WithError(err).Error("unable to read config file")
		return err
	}

	return nil
}

// UpdateConfig by writing it to the config file.
func UpdateConfig() error {
	return viper.WriteConfig()
}

// GetString returns the string value of the key.
func GetString(key string) string {
	return viper.GetString(key)
}

// GetInt returns the int value of the key.
func GetInt(key string) int {
	return viper.GetInt(key)
}

// GetInt32 returns the int32 value of the key.
func GetInt32(key string) int32 {
	return viper.GetInt32(key)
}

// GetInt64 returns the int64 value of the key.
func GetInt64(key string) int64 {
	return viper.GetInt64(key)
}

// GetFloat64 returns the float64 value of the key.
func GetFloat64(key string) float64 {
	return viper.GetFloat64(key)
}

// GetBool returns the bool value of the key.
func GetBool(key string) bool {
	return viper.GetBool(key)
}

// Set a config value of val to a key.
func Set(key string, val interface{}) {
	viper.Set(key, val)
}

// GetPath works the same as GetString except it runs the fs.ExpandHome function
// on it, expanding any homedir characters to a full absolute path.
// Any errors are raised as a panic.
func GetPath(key string) string {
	path, err := fs.ExpandHome(GetString(key))
	if err != nil {
		panic(err)
	}
	return path
}
