package logger

import (
	"io/ioutil"
	"log/syslog"
	"os"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
	"gitlab.com/slax0rr/lockbox/internal/fs"
)

type LoggerOption func() error

func WithLogFile(logfile string) LoggerOption {
	return func() error {
		logfile, err := fs.ExpandHome(logfile)
		if err != nil {
			logrus.WithError(err).
				Warning("unable to determine home directory for log file")
			return err
		}

		f, err := os.OpenFile(logfile,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		if err != nil {
			logrus.WithError(err).
				WithField("logfile", logfile).
				Warning("unable to open log file for writting, falling back to stdout")
			return err
		}
		logrus.SetOutput(f)
		return nil
	}
}

func WithNoOutput() LoggerOption {
	return func() error {
		logrus.SetOutput(ioutil.Discard)
		return nil
	}
}

func WithSysLog(level syslog.Priority, tag string) LoggerOption {
	return func() error {
		hook, err := lSyslog.NewSyslogHook("", "", level, tag)
		if err != nil {
			return err
		}

		logrus.AddHook(hook)
		return nil
	}
}

func WithLogLevel(level logrus.Level) LoggerOption {
	return func() error {
		logrus.SetLevel(level)
		return nil
	}
}

func WithJSONFormatter(timestamp bool) LoggerOption {
	return func() error {
		logrus.SetFormatter(&logrus.JSONFormatter{DisableTimestamp: !timestamp})
		return nil
	}
}

func WithTextFormatter(colors, timestamp bool) LoggerOption {
	return func() error {
		logrus.SetFormatter(&logrus.TextFormatter{DisableColors: !colors,
			DisableTimestamp: !timestamp})
		return nil
	}
}

func Init(opts ...LoggerOption) error {
	for _, opt := range opts {
		if err := opt(); err != nil {
			return err
		}
	}

	return nil
}

// TODO(slax0rr): implement the logrus.FieldLogger interface functions
