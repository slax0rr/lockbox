package auth_test

import (
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/auth"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestClaimsValidate(t *testing.T) {
	Convey("Validating token claims", t, func() {
		sqlDB, mock, err := sqlmock.New()
		So(err, ShouldBeNil)

		db, err := gorm.Open(sqlite.Dialector{Conn: sqlDB}, &gorm.Config{})
		So(err, ShouldBeNil)

		err = auth.Init(db, auth.WithSigningMethod(jwt.SigningMethodHS384.Name),
			auth.WithSigningSecret([]byte("test")))
		So(err, ShouldBeNil)

		expQuery := "^SELECT count\\(1\\) FROM `devices`"

		Convey("that have no validation issues", func() {
			deviceID := uuid.New()
			mock.ExpectQuery(expQuery).
				WithArgs(deviceID.String()).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(1)))

			claims := auth.Claims{
				StandardClaims: jwt.StandardClaims{
					Audience:  "lockbox-hub",
					ExpiresAt: time.Now().Add(10 * time.Second).Unix(),
					Issuer:    "lockbox",
					NotBefore: time.Now().Add(-10 * time.Second).Unix(),
					Subject:   deviceID.String(),
				},
			}

			err := claims.Valid()
			So(err, ShouldBeNil)
		})

		Convey("with a missing device ID", func() {
			deviceID := uuid.New()
			mock.ExpectQuery(expQuery).
				WithArgs(deviceID.String()).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(0)))

			claims := auth.Claims{
				StandardClaims: jwt.StandardClaims{
					Audience:  "lockbox-hub",
					ExpiresAt: time.Now().Add(10 * time.Second).Unix(),
					Issuer:    "lockbox",
					NotBefore: time.Now().Add(-10 * time.Second).Unix(),
					Subject:   deviceID.String(),
				},
			}

			err := claims.Valid()
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "validating token claims: device not found")
		})

		Convey("with expires_at in the past", func() {
			claims := auth.Claims{
				StandardClaims: jwt.StandardClaims{
					Audience:  "lockbox-hub",
					ExpiresAt: time.Now().Add(-10 * time.Second).Unix(),
				},
			}

			err := claims.Valid()
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "validating token claims: token expired or expiration missing")
		})
	})
}

func TestTokenValidation(t *testing.T) {
	Convey("Validating a JWT token", t, func() {
		Convey("that is using an unsafe `none` alg", func() {
			tkn := "eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJhdWQiOiJsb2NrYm94LXNlcnZlciIsImV4cCI6MTYyMzIyMjk1NiwiaWF0IjoxNjIzMDUwMTU2LCJpc3MiOiJsb2NrYm94IiwibmJmIjoxNjIzMDUwMTU2LCJzdWIiOiI4MzVkM2Y4OS1kZjI2LTQzOTktYjk1OS1hMjcyZjRkZDZiYzEifQ."

			_, err := auth.ValidateToken(tkn)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "parsing token with claims: 'none' signature type is not allowed")
		})

		Convey("that was just issued", func() {
			sqlDB, mock, err := sqlmock.New()
			So(err, ShouldBeNil)

			db, err := gorm.Open(sqlite.Dialector{Conn: sqlDB}, &gorm.Config{})
			So(err, ShouldBeNil)

			err = auth.Init(db, auth.WithSigningMethod(jwt.SigningMethodHS384.Name),
				auth.WithSigningSecret([]byte("test")))
			So(err, ShouldBeNil)

			expQuery := "^SELECT count\\(1\\) FROM `devices`"

			deviceID := uuid.New()
			tkn, err := auth.NewToken(&auth.Claims{DeviceID: deviceID.String()})
			So(err, ShouldBeNil)

			mock.ExpectQuery(expQuery).
				WithArgs(deviceID.String()).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(1)))

			claims, err := auth.ValidateToken(tkn)
			So(err, ShouldBeNil)
			So(claims.DeviceID, ShouldEqual, deviceID.String())
		})
	})
}
