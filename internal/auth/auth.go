package auth

import (
	"fmt"
	"io/ioutil"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"gorm.io/gorm"
)

const (
	tokenIssuer   string = "lockbox"
	tokenAudience string = "lockbox-hub"
)

type Claims struct {
	jwt.StandardClaims

	DeviceID string `json:"device_id,omitempty"`
}

func (c Claims) Validate() error {
	now := time.Now().UTC().Unix()

	if !c.StandardClaims.VerifyAudience(tokenAudience, true) {
		return errors.New("audience missing or missmatched")
	}

	if !c.StandardClaims.VerifyExpiresAt(now, true) {
		return errors.New("token expired or expiration missing")
	}

	if !c.StandardClaims.VerifyIssuer(tokenIssuer, true) {
		return errors.New("invalid or missing issuer")
	}

	if !c.StandardClaims.VerifyNotBefore(now, true) {
		return errors.New("token used before designated timestamp or `nbf` missing")
	}

	var records int64
	result := cfg.db.Model(&models.DeviceDB{}).Where("uuid = ?", c.StandardClaims.Subject).
		Count(&records)
	if result.Error != nil {
		return errors.Wrap(result.Error, "unable to check existance of device")
	}
	if records == 0 {
		return errors.New("device not found")
	}

	return nil
}

func (c Claims) Valid() error {
	if _, err := uuid.Parse(c.StandardClaims.Subject); err != nil {
		errors.Wrap(err, "device uuid unparsable")
	}

	if err := c.Validate(); err != nil {
		return errors.Wrap(err, "validating token claims")
	}

	return nil
}

var (
	cfg *config
)

type config struct {
	db            *gorm.DB
	signingSecret []byte
	signingMethod jwt.SigningMethod
	expiration    time.Duration
}

type Option func(*config) error

func WithSigningSecret(sec []byte) Option {
	return func(c *config) error {
		c.signingSecret = sec
		return nil
	}
}

func WithSigningSecretFile(keyFile string) Option {
	return func(c *config) error {
		f, err := fs.OpenFile(keyFile)
		if err != nil {
			return errors.Wrap(err, "unable to open key file")
		}

		c.signingSecret, err = ioutil.ReadAll(f)
		if err != nil {
			return errors.Wrap(err, "unable to read key file")
		}
		return nil
	}
}

func WithSigningMethod(method string) Option {
	return func(c *config) error {
		c.signingMethod = jwt.GetSigningMethod(method)
		if c.signingMethod == nil {
			return fmt.Errorf("signing method (%s) is invalid", method)
		}
		return nil
	}
}

func WithTokenExpiration(exp int64) Option {
	return func(c *config) error {
		c.expiration = time.Duration(exp) * time.Minute
		return nil
	}
}

func Init(db *gorm.DB, opts ...Option) error {
	cfg = &config{db: db}

	for _, opt := range opts {
		if err := opt(cfg); err != nil {
			return errors.Wrap(err, "failed applying options")
		}
	}

	if cfg.signingMethod == nil {
		return errors.New("signing method is not set")
	}

	if cfg.signingSecret == nil {
		return errors.New("no signing secret key set")
	}

	if cfg.expiration == 0 {
		cfg.expiration = 48 * time.Hour
	}

	return nil
}

func NewToken(claims *Claims) (string, error) {
	if claims != nil {
		claims.IssuedAt = time.Now().UTC().Unix()
		claims.ExpiresAt = time.Now().UTC().Add(cfg.expiration).Unix()
		claims.NotBefore = time.Now().UTC().Unix()
		claims.Audience = "lockbox-hub"
		claims.Issuer = tokenIssuer
		claims.Subject = claims.DeviceID
	}

	token := jwt.NewWithClaims(cfg.signingMethod, claims.StandardClaims)
	tokenString, err := token.SignedString(cfg.signingSecret)
	if err != nil {
		return "", errors.Wrap(err, "unable to sign token")
	}

	return tokenString, nil
}

// ValidateToken verifies that the given token is not expired and valid and
// returns the token claims.
func ValidateToken(tokenString string) (*Claims, error) {
	claims := Claims{}
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		return cfg.signingSecret, nil
	}
	_, err := jwt.ParseWithClaims(tokenString, &claims, keyFunc)
	if err != nil {
		return nil, errors.Wrap(err, "parsing token with claims")
	}

	claims.DeviceID = claims.StandardClaims.Subject

	return &claims, err
}
