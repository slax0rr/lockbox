package lockbox

import (
	"bytes"
	"io/ioutil"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
)

type Password struct {
	name      string
	path      string
	data      *bytes.Buffer
	encrypted bool
}

// NewPassword creates a new password object, sets its name, and the
// password_path from the config as its base path.
func NewPassword(name string) *Password {
	p := new(Password)
	p.path = config.GetPath("general.password_path")
	p.name = name
	return p
}

// SetPath as base path for the passwords.
func (p *Password) SetPath(path string) {
	p.path = path
}

// Exists checks if the password already exists in the lockbox general path.
func (p *Password) Exists() bool {
	err := fs.FileExists(filepath.Join(p.path, p.name))
	if errors.Is(err, fs.ErrFileNotExists) {
		return false
	} else if err != nil {
		logrus.WithError(err).
			Panic("unable to determine if password file exists")
	}
	return true
}

// ReadFromFile reads the password from the file under the path and writes it
// to the data buffer, automatically marking it as encrypted.
func (p *Password) ReadFromFile() error {
	f, err := fs.OpenFile(filepath.Join(p.path, p.name))
	if err != nil {
		logrus.WithError(err).Error("unable to open password file")
		return err
	}

	data, err := ioutil.ReadAll(f)
	if err != nil {
		logrus.WithError(err).Error("unable to read password file")
		return err
	}

	p.data = new(bytes.Buffer)
	_, err = p.data.Write(data)
	if err != nil {
		logrus.WithError(err).Error("unable to write password data to object")
		return err
	}

	p.encrypted = true

	return nil
}

// WritetoFile attempts to write the password to its file. If the password is
// not encrypted an error is returned.
func (p *Password) WriteToFile() error {
	if !p.encrypted {
		return errors.New("attempting to write unencrypted password")
	}

	f, err := fs.CreateFileWithPath(filepath.Join(p.path, p.name), 0700)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(p.data.Bytes())
	if err != nil {
		return err
	}

	return nil
}

// Write the received data to the object.
func (p *Password) Write(data []byte) (n int, err error) {
	p.data = bytes.NewBuffer(data)
	return p.data.Len(), nil
}

// Close the password by encrypting it and writing it to the file.
func (p *Password) Close() error {
	err := p.Encrypt()
	if err != nil {
		return err
	}
	return p.WriteToFile()
}

// Encrypt the password.
func (p *Password) Encrypt() error {
	if p.encrypted {
		return nil
	}

	encrypted := new(bytes.Buffer)
	err := crypto.Encrypt(p.data, encrypted)
	if err != nil {
		return err
	}

	p.data = new(bytes.Buffer)
	err = crypto.ArmorEncode(encrypted, p.data)
	if err != nil {
		return err
	}

	p.encrypted = true

	return nil
}

// Decrypt the password.
func (p *Password) Decrypt() error {
	if !p.encrypted {
		return nil
	}

	if err := crypto.ArmorDecode(p.data, p.data); err != nil {
		return errors.Wrap(err, "failed to armor-decode password data")
	}

	if err := crypto.Decrypt(p.data, p.data); err != nil {
		return errors.Wrap(err, "password decryption failed")
	}

	p.encrypted = false

	return nil
}

// Bytes returns the data as a byte slice.
func (p *Password) Bytes() []byte {
	return p.data.Bytes()
}

// String returns the data as a string.
func (p *Password) String() string {
	return p.data.String()
}

// Name returns the name of the secret.
func (p *Password) Name() string {
	return p.name
}
