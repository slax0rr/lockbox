package lockbox_test

import (
	"os"
	"testing"

	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
)

const (
	generalPath  = "/tmp/lockbox-test/"
	passwordPath = "/tmp/lockbox-test/data/"
)

func TestMain(m *testing.M) {
	err := fs.CreateDir(passwordPath, 0700, true)
	if err != nil {
		panic(err)
	}

	config.Set("general.path", generalPath)
	config.Set("general.password_path", passwordPath)
	crypto.SetRSABits(1024) // good enough for test
	_, err = crypto.GeneratePrimaryKey("foo bar", "foo@bar.com", "foobar")
	if err != nil {
		panic(err)
	}

	ret := m.Run()

	err = os.RemoveAll(generalPath)
	if err != nil {
		panic(err)
	}

	os.Exit(ret)
}
