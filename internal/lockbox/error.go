package lockbox

type LockboxError string

func (e LockboxError) Error() string {
	return string(e)
}

const (
	ErrKeysNotFound   LockboxError = "Encryption keys are not found"
	ErrPasswordExists LockboxError = "password already exists"
	ErrAuthFailed     LockboxError = "authentication failed"
	ErrRegisterFailed LockboxError = "registration failed"
	ErrUnknownDevice  LockboxError = "requested device is unknown or does not exist"
	ErrInternalError  LockboxError = "unexpected internal error occurred"
)
