package hub

import (
	"context"
	"errors"
	"log/syslog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	apierrors "github.com/go-openapi/errors"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/auth"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/handler"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"gitlab.com/slax0rr/lockbox/internal/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	defaultPath       string = "/etc/lockbox/"
	defaultConfigFile string = "lockbox.toml"
)

type lockboxHub struct {
	pb.UnimplementedLockboxHubServer

	cfgFile string
	db      *gorm.DB
	srv     *grpc.Server
}

type LockboxOpt func(*lockboxHub)

func WithConfigFile(cfgFile string) LockboxOpt {
	return func(l *lockboxHub) {
		if cfgFile != "" {
			l.cfgFile = cfgFile
		}
	}
}

func Init(api *operations.LockboxAPI, opts ...LockboxOpt) *lockboxHub {
	hub := &lockboxHub{
		cfgFile: defaultPath + defaultConfigFile,
	}

	for _, opt := range opts {
		opt(hub)
	}

	err := config.ReadConfig(hub.cfgFile)
	if err != nil {
		panic(err)
	}

	err = initLogger()
	if err != nil {
		panic(err)
	}
	api.Logger = logrus.StandardLogger().Printf

	hub.db, err = initDatabase()
	if err != nil {
		panic(err)
	}

	err = initAuth(hub)
	if err != nil {
		panic(err)
	}

	api.KeyAuth = func(token string) (*models.Principal, error) {
		unauthorized := apierrors.New(http.StatusUnauthorized, "Unauthorized")
		if !strings.HasPrefix(strings.ToLower(token), "bearer") {
			logrus.Debug("invalid token format")
			return nil, unauthorized
		}

		claims, err := auth.ValidateToken(token[7:])
		if err != nil {
			logrus.WithError(err).Debug("token validation failed")
			return nil, unauthorized
		}

		return (*models.Principal)(&claims.DeviceID), nil
	}

	hub.registerHandlers(api)

	go startRPCServer(hub)

	logrus.Debug("server initialised")

	return hub
}

func (hub *lockboxHub) Shutdown() {
	if hub.db != nil {
		sql, err := hub.db.DB()
		if err == nil {
			err := sql.Close()
			if err != nil {
				logrus.WithError(err).Error("closing database connection failed")
			}
		} else {
			logrus.WithError(err).Error("unable to retrieve underlying sql connection")
		}
	}
}

func initLogger() error {
	logLevel, err := logrus.ParseLevel(config.GetString("log.level"))
	if err != nil {
		return err
	}

	loggerOpts := []logger.LoggerOption{
		logger.WithLogLevel(logLevel),
	}

	if !config.GetBool("log.output") {
		loggerOpts = append(loggerOpts, logger.WithNoOutput())
	}

	if config.GetBool("log.syslog") {
		loggerOpts = append(loggerOpts, logger.WithTextFormatter(false, false),
			logger.WithSysLog(syslog.LOG_DEBUG, "lockbox-hub"))
	}

	err = logger.Init(loggerOpts...)
	if err != nil {
		return err
	}

	return nil
}

func initDatabase() (*gorm.DB, error) {
	db, err := gorm.Open(sqlite.Open(config.GetPath("database.addr")), &gorm.Config{})
	if err != nil {
		logrus.WithError(err).Error("unable to establish database connection")
		return nil, err
	}

	err = db.AutoMigrate(&models.Account{}, &models.DeviceDB{})
	if err != nil {
		logrus.WithError(err).Error("migrations failed")
		return nil, err
	}

	return db, nil
}

func initAuth(hub *lockboxHub) error {
	opts := []auth.Option{
		auth.WithSigningMethod(config.GetString("auth.signing_method")),
	}

	if keyFile := config.GetPath("auth.secret_key_file"); keyFile != "" {
		opts = append(opts, auth.WithSigningSecretFile(keyFile))
	} else {
		key := []byte(config.GetString("auth.secret_key"))
		opts = append(opts, auth.WithSigningSecret(key))
	}

	err := auth.Init(hub.db, opts...)
	if err != nil {
		return err
	}

	return nil
}

// authenticate checks the authentication and returns the device ID of the
// authenticated device.
func (l lockboxHub) authenticate(ctx context.Context) (*uuid.UUID, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, errors.New("unable to extract metadata from context")
	}

	t := md.Get("authorization")
	if len(t) == 0 {
		return nil, errors.New("authorization header missing")
	}
	token := t[0]

	if !strings.HasPrefix(strings.ToLower(token), "bearer") {
		logrus.Debug("invalid token format")
		return nil, lockbox.ErrAuthFailed
	}

	claims, err := auth.ValidateToken(token[7:])
	if err != nil {
		logrus.WithError(err).Debug("token validation failed")
		return nil, lockbox.ErrAuthFailed
	}

	deviceUUID, err := uuid.Parse(claims.DeviceID)
	if err != nil {
		logrus.WithError(err).Error("failed parsing device UUID")
		return nil, lockbox.ErrInternalError
	}

	return &deviceUUID, nil
}

func (l *lockboxHub) SendClientKey(ctx context.Context, clientKey *pb.ClientKey) (*empty.Empty, error) {
	deviceID, err := l.authenticate(ctx)
	if err != nil {
		return &empty.Empty{}, status.Error(codes.Unauthenticated, err.Error())
	}

	deviceHandler := handler.NewDeviceHandler(l.db)
	device, err := deviceHandler.GetDevice(ctx, deviceID.String())
	if err != nil {
		return nil, err
	}

	ev := &pb.Event{
		Type: pb.Event_PUBKEY_UPDATE,
		Device: &pb.Device{
			Id:   device.UUID,
			Name: device.Name,
		},
		DeviceClientKey: &pb.ClientKey{
			PublicKey: clientKey.GetPublicKey(),
		},
	}
	sendToOthers(device.AccountEmail, *deviceID, ev)

	err = deviceHandler.SetDeviceKey(ctx, *deviceID, clientKey.PublicKey)
	if err != nil {
		return &empty.Empty{}, err
	}

	return &empty.Empty{}, nil
}

func (l *lockboxHub) Pipeline(stream pb.LockboxHub_PipelineServer) error {
	ctx := stream.Context()
	deviceID, err := l.authenticate(stream.Context())
	if err != nil {
		return status.Error(codes.Unauthenticated, err.Error())
	}

	device, err := handler.NewDeviceHandler(l.db).GetDevice(stream.Context(), deviceID.String())
	if err != nil {
		return err
	}

	c := make(chan *pb.Event, 10)

	registerPipeline(device.AccountEmail, *deviceID, c)
	defer unregisterPipeline(device.AccountEmail, *deviceID)

	go func() {
		for {
			ev, err := stream.Recv()
			if err != nil {
				logrus.WithError(err).Error("receive event from pipeline")
				return
			}
			logrus.WithField("event", ev.GetType()).Info("received event on pipeline")
			sendToOthers(device.AccountEmail, *deviceID, ev)
		}
	}()

	for {
		var event *pb.Event
		select {
		case event = <-c:
			if event == nil {
				logrus.Debug("channel was closed")
				return nil
			}

		case <-ctx.Done():
			logrus.WithError(ctx.Err()).Error("context done")
			return ctx.Err()
		}

		logrus.WithField("event", event.Type).Info("sending event")

		if err := stream.Send(event); err != nil {
			logrus.WithError(err).Error("could not write message to stream")
			return status.Error(codes.Internal, lockbox.ErrInternalError.Error())
		}
	}
}

func waitForSignals(srv *grpc.Server) {
	sig := make(chan os.Signal, 1024)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT)
	for {
		select {
		case s := <-sig:
			switch s {
			case syscall.SIGHUP:
				srv.GracefulStop()
				return

			case syscall.SIGTERM, syscall.SIGINT:
				srv.Stop()
				return

			default:
				logrus.WithField("signal", s).Debug("unhandled signal caught")
			}
		}
	}
}

func startRPCServer(hub *lockboxHub) error {
	// TODO(slax0rr): read listener port from config
	lis, err := net.Listen("tcp", ":9369")
	if err != nil {
		logrus.WithError(err).Error("unable to start TCP listener")
		return err
	}

	hub.srv = grpc.NewServer()
	pb.RegisterLockboxHubServer(hub.srv, hub)
	go func(s *grpc.Server) {
		err = s.Serve(lis)
		if err != nil {
			logrus.WithError(err).Panic("unable to start gRPC server")
		}
	}(hub.srv)

	waitForSignals(hub.srv)
	logrus.Debug("gRPC server stopped")

	return nil
}

func (l lockboxHub) registerHandlers(api *operations.LockboxAPI) {
	api.HealthGetHealthHandler = handler.NewGetHealthHandler(l.db)
	api.AccountPostAccountHandler = handler.NewPostAccountHandler(l.db)
	api.AuthenticationAuthUserHandler = handler.NewPostAuthHandler(l.db)
}
