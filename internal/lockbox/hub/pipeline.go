package hub

import (
	"sync"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
)

var pipelines map[string]map[uuid.UUID]chan *pb.Event
var mu sync.Mutex

func init() {
	pipelines = make(map[string]map[uuid.UUID]chan *pb.Event)
}

func registerPipeline(accountEmail string, u uuid.UUID, c chan *pb.Event) {
	mu.Lock()
	defer mu.Unlock()

	deviceMap, ok := pipelines[accountEmail]
	if !ok {
		deviceMap = make(map[uuid.UUID]chan *pb.Event)
	}

	deviceMap[u] = c
	pipelines[accountEmail] = deviceMap
}

func unregisterPipeline(accountEmail string, uuid uuid.UUID) {
	mu.Lock()
	defer mu.Unlock()

	_, ok := pipelines[accountEmail]
	if !ok {
		return
	}

	delete(pipelines[accountEmail], uuid)
}

func sendToOthers(accountEmail string, uuid uuid.UUID, ev *pb.Event) {
	_, ok := pipelines[accountEmail]
	if !ok {
		return
	}

	for u, c := range pipelines[accountEmail] {
		if u == uuid {
			continue
		}

		select {
		case c <- ev:

		default:
			logrus.WithField("device_uuid", uuid).
				Warn("no listener or all listeners busy on device channel")
		}
	}
}
