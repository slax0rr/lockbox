package handler_test

import (
	"database/sql/driver"
	"os"
	"testing"
	"time"

	"gitlab.com/slax0rr/lockbox/internal/config"
)

type AnyTime struct{}

func (AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func TestMain(m *testing.M) {
	config.ReadConfig("../../../../resources/config/lockbox-hub-dev.toml")

	ret := m.Run()

	os.Exit(ret)
}
