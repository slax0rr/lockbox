package handler

import (
	"context"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type DeviceHandler struct {
	db *gorm.DB
}

func NewDeviceHandler(db *gorm.DB) DeviceHandler {
	return DeviceHandler{db}
}

func (h DeviceHandler) GetDevice(ctx context.Context, deviceID string) (*models.DeviceDB, error) {
	device := &models.DeviceDB{}
	result := h.db.First(device, "uuid = ? ", deviceID)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil, status.Error(codes.NotFound, result.Error.Error())
	}

	return device, nil
}

// GetAllOtherDevices retrieves all other devices that belong to the account of
// deviceUUID.
func (h DeviceHandler) GetAllOtherDevices(ctx context.Context, deviceUUID *uuid.UUID) ([]models.DeviceDB, error) {
	if deviceUUID == nil {
		return nil, errors.New("unable to retreive other devices belonging to nil deviceUUID")
	}
	var devices []models.DeviceDB
	nestedTX := h.db.Table(models.DeviceDB{}.TableName()).
		Select("account_email").
		Where("uuid = ?", deviceUUID)

	result := h.db.Where("account_email = ? AND uuid != ?", nestedTX, deviceUUID).
		Find(&devices)

	if result.Error != nil {
		return nil, errors.Wrap(result.Error, "unable to find devices")
	}

	return devices, nil
}

func (h DeviceHandler) SetDeviceKey(ctx context.Context, deviceID uuid.UUID, key string) error {
	device, err := h.GetDevice(ctx, deviceID.String())
	if err != nil {
		logrus.WithError(err).Info("failed loading device")
		return err
	}

	device.PublicKey = key
	result := h.db.Save(device)
	if result.Error != nil {
		logrus.WithError(result.Error).Error("unable to save device with updated key")
		return status.Error(codes.Internal, lockbox.ErrInternalError.Error())
	}

	return nil
}
