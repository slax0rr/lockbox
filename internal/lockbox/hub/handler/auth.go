package handler

import (
	"errors"

	"github.com/go-openapi/runtime/middleware"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/auth"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/authentication"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type PostAuthHandler struct {
	db *gorm.DB
}

func NewPostAuthHandler(db *gorm.DB) PostAuthHandler {
	return PostAuthHandler{db: db}
}

func (h PostAuthHandler) Handle(params authentication.AuthUserParams) middleware.Responder {
	account := models.Account{}

	result := h.db.Take(&account, "email = ?", *params.Body.Credentials.Email)
	if result.Error != nil {
		logrus.WithError(result.Error).Error("account retrieval failed")
		return authentication.NewAuthUserUnauthorized()
	}

	err := bcrypt.CompareHashAndPassword([]byte(account.Password),
		[]byte(params.Body.Credentials.Password.String()))
	if err != nil {
		logrus.WithError(err).Debug("password missmatch")
		return authentication.NewAuthUserUnauthorized()
	}

	if params.Body.Device == nil ||
		params.Body.Device.ID == nil ||
		*params.Body.Device.ID == uuid.Nil.String() {
		logrus.Debug("creating new device")

		err := h.newDevice(*params.Body.Credentials.Email, params.Body.Device)
		if err != nil {
			return authentication.NewAuthUserInternalServerError()
		}
	} else {
		logrus.Debug("check validity of device")
		err := h.deviceValid(*params.Body.Credentials.Email, params.Body.Device)
		if err != nil {
			return authentication.NewAuthUserUnauthorized()
		}
	}

	token, err := auth.NewToken(&auth.Claims{DeviceID: *params.Body.Device.ID})
	if err != nil {
		logrus.WithError(err).Error("token creation failed")
		return authentication.NewAuthUserInternalServerError()
	}

	resp := models.AuthResponse{
		Token:  &models.Token{Token: &token},
		Device: params.Body.Device,
	}

	return authentication.NewAuthUserOK().
		WithPayload(&resp)
}

func (h PostAuthHandler) newDevice(email string, device *models.Device) error {
	var records int64
	result := h.db.Model(&models.DeviceDB{}).
		Where("account_email = ?", email).
		Count(&records)
	if result.Error != nil {
		logrus.WithError(result.Error).Error("retrieval of account devices failed")
		return result.Error
	}

	/*
	 *if records > 0 {
	 *    // TODO(slax0rr): account already has some devices, existing devices
	 *    // must confirm new device.
	 *    logrus.Error("account has existing device")
	 *    return errors.New("account already has connected device")
	 *}
	 */

	deviceID := uuid.New().String()
	device.ID = &deviceID

	deviceDB := models.DeviceDB{
		UUID:         *device.ID,
		AccountEmail: email,
		Type:         *device.Type,
		Name:         *device.Name,
	}
	result = h.db.Create(&deviceDB)
	if result.Error != nil {
		logrus.WithError(result.Error).Error("device creation failed")
		return result.Error
	}

	return nil
}

func (h PostAuthHandler) deviceValid(email string, device *models.Device) error {
	var records int64
	result := h.db.Model(&models.DeviceDB{}).
		Where("account_email = ? AND uuid = ?", email, *device.ID).
		Count(&records)
	if result.Error != nil {
		logrus.WithError(result.Error).
			Error("unable to check existance and association of device")
		return result.Error
	}

	if records == 0 {
		return errors.New("device does not exist or does not belong to the account")
	}

	return nil
}
