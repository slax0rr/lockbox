package handler_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/auth"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/handler"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/authentication"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestPostAuthHandler(t *testing.T) {
	Convey("Authenticate handler is called", t, func() {
		recorder := httptest.NewRecorder()
		sqlDB, mock, err := sqlmock.New()
		So(err, ShouldBeNil)

		db, err := gorm.Open(sqlite.Dialector{Conn: sqlDB}, &gorm.Config{})
		So(err, ShouldBeNil)

		err = auth.Init(db, auth.WithSigningMethod(jwt.SigningMethodHS256.Name),
			auth.WithSigningSecret([]byte("test")))
		So(err, ShouldBeNil)

		Convey("with valid credentials and existing device", func() {
			email := "valid@user.com"
			pass := "validpass"
			deviceName := "test-device"
			deviceType := "cli"
			deviceID := uuid.New().String()
			hashedPass, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			So(err, ShouldBeNil)

			params := authentication.NewAuthUserParams()
			params.Body = &models.Auth{
				Credentials: &models.Credentials{
					Email:    &email,
					Password: (*strfmt.Password)(&pass),
				},
				Device: &models.Device{
					ID:   &deviceID,
					Type: &deviceType,
					Name: &deviceName,
				},
			}

			mock.ExpectQuery("^SELECT \\* FROM `accounts`").
				WithArgs(email).
				WillReturnRows(sqlmock.NewRows([]string{
					"email",
					"password",
				}).AddRow(email, hashedPass))

			mock.ExpectQuery("^SELECT count\\(1\\) FROM `devices`").
				WithArgs(email, deviceID).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(1)))

			mock.ExpectQuery("^SELECT count\\(1\\) FROM `devices`").
				WithArgs(deviceID).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(1)))

			handler.NewPostAuthHandler(db).
				Handle(params).
				WriteResponse(recorder, runtime.JSONProducer())

			So(recorder.Code, ShouldEqual, http.StatusOK)

			authResp := models.AuthResponse{}
			err = json.Unmarshal(recorder.Body.Bytes(), &authResp)
			So(err, ShouldBeNil)

			So(authResp.Device.ID, ShouldNotBeNil)
			So(*authResp.Device.ID, ShouldEqual, deviceID)

			So(authResp.Token.Token, ShouldNotBeNil)
			_, err = auth.ValidateToken(*authResp.Token.Token)
			So(err, ShouldBeNil)

			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})

		Convey("with valid credentials and new device", func() {
			email := "valid@user.com"
			pass := "validpass"
			deviceName := "test-device"
			deviceType := "cli"
			deviceKey := ""
			deviceID := uuid.Nil.String()
			hashedPass, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			So(err, ShouldBeNil)

			params := authentication.NewAuthUserParams()
			params.Body = &models.Auth{
				Credentials: &models.Credentials{
					Email:    &email,
					Password: (*strfmt.Password)(&pass),
				},
				Device: &models.Device{
					ID:   &deviceID,
					Type: &deviceType,
					Name: &deviceName,
				},
			}

			mock.ExpectQuery("^SELECT \\* FROM `accounts`").
				WithArgs(email).
				WillReturnRows(sqlmock.NewRows([]string{
					"email",
					"password",
				}).AddRow(email, hashedPass))

			mock.ExpectQuery("^SELECT count\\(1\\) FROM `devices`").
				WithArgs(email).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(0)))

			mock.ExpectExec("INSERT INTO `devices` ").
				WithArgs(sqlmock.AnyArg(), email, deviceType, deviceName, deviceKey, AnyTime{}, AnyTime{}, nil).
				WillReturnResult(sqlmock.NewResult(1, 1))

			mock.ExpectQuery("^SELECT count\\(1\\) FROM `devices`").
				WithArgs(sqlmock.AnyArg()).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(1)))

			handler.NewPostAuthHandler(db).
				Handle(params).
				WriteResponse(recorder, runtime.JSONProducer())

			So(recorder.Code, ShouldEqual, http.StatusOK)

			authResp := models.AuthResponse{}
			err = json.Unmarshal(recorder.Body.Bytes(), &authResp)
			So(err, ShouldBeNil)

			So(authResp.Device.ID, ShouldNotBeNil)
			So(*authResp.Device.ID, ShouldNotEqual, deviceID)

			So(authResp.Token.Token, ShouldNotBeNil)
			_, err = auth.ValidateToken(*authResp.Token.Token)
			So(err, ShouldBeNil)

			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})

		Convey("with invalid credentials", func() {
			email := "invalid@user.com"
			pass := "invalidpass"

			deviceID := uuid.New().String()
			So(err, ShouldBeNil)

			params := authentication.NewAuthUserParams()
			params.Body = &models.Auth{
				Credentials: &models.Credentials{
					Email:    &email,
					Password: (*strfmt.Password)(&pass),
				},
				Device: &models.Device{
					ID: &deviceID,
				},
			}

			mock.ExpectQuery("^SELECT \\* FROM `accounts`").
				WithArgs(email).
				WillReturnRows(sqlmock.NewRows([]string{
					"email",
					"password",
				}))

			handler.NewPostAuthHandler(db).
				Handle(params).
				WriteResponse(recorder, runtime.JSONProducer())

			So(recorder.Code, ShouldEqual, http.StatusUnauthorized)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})

		Convey("with valid credentials and device not owned by the account", func() {
			email := "valid@user.com"
			pass := "validpass"
			deviceID := uuid.New().String()
			hashedPass, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			So(err, ShouldBeNil)

			params := authentication.NewAuthUserParams()
			params.Body = &models.Auth{
				Credentials: &models.Credentials{
					Email:    &email,
					Password: (*strfmt.Password)(&pass),
				},
				Device: &models.Device{
					ID: &deviceID,
				},
			}

			mock.ExpectQuery("^SELECT \\* FROM `accounts`").
				WithArgs(email).
				WillReturnRows(sqlmock.NewRows([]string{
					"email",
					"password",
				}).AddRow(email, hashedPass))

			mock.ExpectQuery("^SELECT count\\(1\\) FROM `devices`").
				WithArgs(email, deviceID).
				WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(int64(0)))

			handler.NewPostAuthHandler(db).
				Handle(params).
				WriteResponse(recorder, runtime.JSONProducer())

			So(recorder.Code, ShouldEqual, http.StatusUnauthorized)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})
	})
}
