package handler

import (
	"github.com/go-openapi/runtime/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/health"
	"gorm.io/gorm"
)

type GetHealthHandler struct {
	db *gorm.DB
}

func NewGetHealthHandler(db *gorm.DB) GetHealthHandler {
	return GetHealthHandler{db: db}
}

func (h GetHealthHandler) Handle(params health.GetHealthParams) middleware.Responder {
	sql, err := h.db.DB()
	if err != nil {
		logrus.WithError(err).Error("unable to retrieve underlying sql connection")
		return health.NewGetHealthInternalServerError()
	}

	if err := sql.Ping(); err != nil {
		logrus.WithError(err).Error("database ping failed")
		return health.NewGetHealthInternalServerError()
	}

	return health.NewGetHealthOK()
}
