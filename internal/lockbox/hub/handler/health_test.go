package handler_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/handler"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/health"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestGetHealthHandler(t *testing.T) {
	Convey("The health handler", t, func() {
		recorder := httptest.NewRecorder()
		sqlDB, mock, err := sqlmock.New(sqlmock.MonitorPingsOption(true))
		So(err, ShouldBeNil)

		db, err := gorm.Open(sqlite.Dialector{Conn: sqlDB},
			&gorm.Config{DisableAutomaticPing: true})
		So(err, ShouldBeNil)

		Convey("responds with OK", func() {
			mock.ExpectPing()

			handler.NewGetHealthHandler(db).
				Handle(health.NewGetHealthParams()).
				WriteResponse(recorder, nil)

			So(recorder.Code, ShouldEqual, http.StatusOK)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})

		Convey("responds with InternalServerError when the database in unreachable", func() {
			mock.ExpectPing().WillReturnError(errors.New("test error"))

			handler.NewGetHealthHandler(db).
				Handle(health.NewGetHealthParams()).
				WriteResponse(recorder, nil)

			So(recorder.Code, ShouldEqual, http.StatusInternalServerError)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})
	})
}
