package handler_test

import (
	"database/sql/driver"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/go-openapi/strfmt"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/handler"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/account"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type HashedPassword struct {
	expectedPass string
}

func (p HashedPassword) Match(v driver.Value) bool {
	hashed, ok := v.(string)
	if !ok {
		return false
	}

	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(p.expectedPass))
	return err == nil
}

func TestPostAccountHandler(t *testing.T) {
	Convey("Create account handler is called", t, func() {
		recorder := httptest.NewRecorder()
		sqlDB, mock, err := sqlmock.New()
		So(err, ShouldBeNil)

		db, err := gorm.Open(sqlite.Dialector{Conn: sqlDB}, &gorm.Config{})
		So(err, ShouldBeNil)

		Convey("and a 201 code is returned when it is created", func() {
			email := "non@existing.com"
			pass := "testpass"

			mock.ExpectExec("INSERT INTO `accounts` ").
				WithArgs(email, HashedPassword{pass}, AnyTime{}, AnyTime{}, nil).
				WillReturnResult(sqlmock.NewResult(1, 1))

			params := account.NewPostAccountParams()
			params.Body = &models.Credentials{
				Email:    &email,
				Password: (*strfmt.Password)(&pass),
			}

			handler.NewPostAccountHandler(db).
				Handle(params).
				WriteResponse(recorder, nil)

			So(recorder.Code, ShouldEqual, http.StatusCreated)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})

		Convey("and a 500 code when the database query fails", func() {
			email := "an@existing.com"
			pass := "testpass"

			mock.ExpectExec("INSERT INTO `accounts` ").
				WithArgs(email, HashedPassword{pass}, AnyTime{}, AnyTime{}, nil).
				WillReturnError(errors.New("test error"))

			params := account.NewPostAccountParams()
			params.Body = &models.Credentials{
				Email:    &email,
				Password: (*strfmt.Password)(&pass),
			}

			handler.NewPostAccountHandler(db).
				Handle(params).
				WriteResponse(recorder, nil)

			So(recorder.Code, ShouldEqual, http.StatusInternalServerError)
			So(mock.ExpectationsWereMet(), ShouldBeNil)
		})
	})
}
