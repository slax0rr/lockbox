package handler

import (
	"strings"

	"github.com/go-openapi/runtime/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/hub/restapi/operations/account"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type PostAccountHandler struct {
	db *gorm.DB
}

func NewPostAccountHandler(db *gorm.DB) PostAccountHandler {
	return PostAccountHandler{db: db}
}

func (h PostAccountHandler) Handle(params account.PostAccountParams) middleware.Responder {
	logrus.Debug("handling post account")

	hashed, err := bcrypt.GenerateFromPassword([]byte(params.Body.Password.String()), bcrypt.DefaultCost)
	if err != nil {
		logrus.WithError(err).Error("hashing of user password failed")
		return account.NewPostAccountInternalServerError()
	}

	accountModel := models.Account{
		Email:    *params.Body.Email,
		Password: string(hashed),
	}

	result := h.db.Create(&accountModel)
	if result.Error != nil {
		logrus.WithError(result.Error).Error("account creation failed")
		if strings.HasPrefix(result.Error.Error(), "UNIQUE constraint failed:") {
			logrus.WithField("email", accountModel.Email).
				Info("account already exists")
		}

		return account.NewPostAccountInternalServerError()
	}

	return account.NewPostAccountCreated()
}
