package daemon_test

import (
	"bytes"
	"context"
	"regexp"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/daemon"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestGetPassword(t *testing.T) {
	Convey("Retrieve a password", t, func() {
		length := int32(10)
		passName := "pass-retrieval"
		genPass, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
			Pass: &pb.Password{
				Name:   passName,
				Length: length,
			},
		})
		So(err, ShouldBeNil)

		passphrase := bytes.NewBufferString("foobar")
		daemon.SetSecretReader(&testSecretReader{passphrase})

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		pass, err := cl.GetPassword(ctx, &pb.Password{
			Name: passName,
		})
		So(err, ShouldBeNil)
		So(pass.GetPass(), ShouldEqual, genPass.GetPass())
	})
}

func TestGeneratePassword(t *testing.T) {
	Convey("Generate password", t, func() {
		Convey("with a defined length", func() {
			length := int32(10)
			pass, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
				Pass: &pb.Password{
					Name:   "pass-length",
					Length: length,
				},
			})
			So(err, ShouldBeNil)
			So(len(pass.GetPass()), ShouldEqual, length)
		})

		Convey("without a defined length", func() {
			length := int32(20)
			config.Set("password.length", length)
			pass, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
				Pass: &pb.Password{
					Name: "pass-no-length",
				},
			})
			So(err, ShouldBeNil)
			So(len(pass.GetPass()), ShouldEqual, length)
		})

		Convey("the first time", func() {
			fs.Remove(passwordPath + "pass-exists")

			pass := &pb.Password{
				Name:   "pass-exists",
				Length: 10,
			}
			_, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
				Pass: pass,
			})
			So(err, ShouldBeNil)

			Convey("a second time without overwrite flag", func() {
				_, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
					Pass: pass,
				})
				So(err, ShouldNotBeNil)
				status, ok := status.FromError(err)
				So(ok, ShouldBeTrue)
				So(status.Code(), ShouldEqual, codes.AlreadyExists)
				So(status.Message(), ShouldEqual, lockbox.ErrPasswordExists)
			})

			Convey("a second time with overwrite flag", func() {
				_, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
					Pass: pass,
					Opts: []*pb.LockboxOption{
						{Val: pb.LockboxOption_OVERWRITE},
					},
				})
				So(err, ShouldBeNil)
			})
		})

		Convey("with a no-symbols flag", func() {
			length := int32(10)
			pass, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
				Pass: &pb.Password{
					Name:   "pass-no-symbols",
					Length: length,
				},
				Opts: []*pb.LockboxOption{
					{Val: pb.LockboxOption_NO_SYMBOLS},
				},
			})
			So(err, ShouldBeNil)

			re := regexp.MustCompile(`\W`)
			So(re.MatchString(pass.GetPass()), ShouldBeFalse)
		})
	})
}
