package daemon

import (
	"bytes"
	"context"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
	"gitlab.com/slax0rr/lockbox/pkg/event"
)

const (
	keyFilename = "primarykey"
)

var (
	deviceKeys map[string]*crypto.Key
)

func init() {
	deviceKeys = make(map[string]*crypto.Key)
	event.RegisterHandler(ev_NEW_HUB_CONNECTION, sendClientKeyHandler)
}

func storeDeviceKey(deviceID string, rawKey []byte) error {
	buf := bytes.NewBuffer(rawKey)
	key, err := crypto.DecodeKey(buf)
	if err != nil {
		return errors.Wrap(err, "decoding device key failed")
	}

	if _, ok := deviceKeys[deviceID]; ok {
		logrus.WithField("device", deviceID).
			Warn("setting device key - key for device already exists")
	}
	deviceKeys[deviceID] = key

	deviceKeyPath := filepath.Join(config.GetPath("general.key_path"), deviceID+".pub")
	logrus.WithField("device_key", deviceKeyPath).Debug("writing device key")
	err = ioutil.WriteFile(deviceKeyPath, rawKey, 0600)
	if err != nil {
		logrus.WithError(err).Error("unable to write primary key to file")
		return errors.Wrapf(err, "could not write to %s", deviceKeyPath)
	}

	return nil
}

func unlockPrivateKey() error {
	passphrase, err := readKeyPassphrase("")
	if err != nil {
		logrus.WithError(err).Error("unable to read passphrase")
		return err
	}

	return crypto.DecryptPrivateKey(passphrase)
}

func initKeys() error {
	if !keyExists() {
		logrus.Info("public and/or private keys do not exist")
		return nil
	}

	if err := loadEncryptionKeys(); err != nil {
		return errors.Wrap(err, "unable to load encryption keys")
	}

	pubKey, err := crypto.ArmorPublicKey()
	if err != nil {
		logrus.WithError(err).Warn("unable to load public key")
		return err
	}
	if currState.setCurrentKeyVersion(pubKey) {
		logrus.Info("current key version updated")
	}

	event.Dispatch(ev_KEYS_INITED)

	return nil
}

func loadEncryptionKeys() error {
	path := config.GetPath("general.key_path")
	privKey, err := fs.OpenFile(filepath.Join(path, keyFilename))
	if err != nil {
		logrus.WithError(err).Error("unable to open private key file")
		return err
	}
	defer privKey.Close()

	if err := crypto.LoadKey(privKey); err != nil {
		logrus.WithError(err).Error("unable to load private key")
		return err
	}

	fs.Walk(path, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		name := filepath.Base(info.Name())
		if name == keyFilename {
			return nil
		}

		rawKey, err := fs.OpenFile(filePath)
		if err != nil {
			return errors.Wrap(err, "error opening key file")
		}
		defer rawKey.Close()

		key, err := crypto.DecodeKey(rawKey)
		if err != nil {
			return errors.Wrap(err, "error decoding device key")
		}

		deviceKeys[name] = key

		return nil
	})

	return nil
}

func createKeyPair(name, email string) error {
	passphrase, err := readKeyPassphrase("")
	if err != nil {
		logrus.WithError(err).Error("unable to read passphrase")
		return err
	}

	_, err = crypto.GeneratePrimaryKey(name, email, string(passphrase))
	if err != nil {
		logrus.WithError(err).Panic("unable to generate key pair")
		return err
	}

	if err := writeKey(); err != nil {
		logrus.WithError(err).Error("unable to write keys")
		return err
	}

	logrus.Debug("keys created and saved")

	return nil
}

func renewKeys(name, email string) error {
	logrus.Debug("starting key renewal")

	newPassPath := filepath.Join(config.GetPath("general.path"), "lockbox_renew")
	fs.RemoveAll(newPassPath)

	if crypto.PrivateKeyEncrypted() {
		oldPassphrase, err := readKeyPassphrase("Enter passphrase of current key")
		if err != nil {
			logrus.WithError(err).Error("unable to read current key passphrase")
			return errors.New("unable to read current key passphrase")
		}

		if err := crypto.DecryptPrivateKey(oldPassphrase); err != nil {
			logrus.WithError(err).Error("unable to decrypt private key")
			return errors.New("unable to decrypt private key")
		}
	}

	passwords, err := readAllPasswords("")
	if err != nil {
		logrus.WithError(err).Error("unable to read and decrypt existing passwords")
		return err
	}

	logrus.Debug("passwords read and decrypted")

	passphrase, err := readKeyPassphrase("Enter passphrase for the new key")
	if err != nil {
		return err
	}

	_, err = crypto.GeneratePrimaryKey(name, email, string(passphrase))
	if err != nil {
		logrus.WithError(err).Error("key pair generation failed")
		return err
	}

	logrus.Debug("new key pair generated")

	for _, p := range passwords {
		p.SetPath(newPassPath)

		err := p.Close()
		if err != nil {
			logrus.WithError(err).Error("unable to re-encrypt and write password")
			return err
		}
	}

	logrus.Debug("passwords re-encrypted with new key")

	err = writeKey()
	if err != nil {
		logrus.WithError(err).Error("unable to write new key pair to filesystem")
		return err
	}

	err = fs.MoveDir(newPassPath, config.GetPath("general.password_path"))
	if err != nil {
		logrus.WithError(err).Error("could not move temp pass path to configured one")
		return err
	}

	logrus.Debug("key renewal complete")

	return nil
}

func keyExists() bool {
	file := filepath.Join(config.GetPath("general.key_path"), keyFilename)

	err := fs.FileExists(file)
	switch {
	case errors.Is(err, fs.ErrFileNotExists):
		logrus.WithField("key", file).Debug("key does not exist")
		return false

	case err != nil:
		logrus.WithError(err).
			Panic("unable to determine if encryption key exists")
	}

	return true
}

func writeKey() error {
	path := config.GetPath("general.key_path")

	privKey, err := crypto.ArmorPrivateKey()
	if err != nil {
		logrus.WithError(err).Error("unable to get armor primary key")
		return err
	}

	err = ioutil.WriteFile(filepath.Join(path, keyFilename), privKey, 0600)
	if err != nil {
		logrus.WithError(err).Error("unable to write primary key to file")
		return err
	}

	return nil
}

func readKeyPassphrase(desc string) ([]byte, error) {
	if desc == "" {
		desc = "Enter passphrase for your private key"
	}
	secretReader.SetPrompt("Lockbox", desc)

	passphrase, err := ioutil.ReadAll(secretReader)
	if err != nil {
		return nil, err
	}

	if len(passphrase) == 0 {
		return nil, errors.New("entered passphrase is of zero length")
	}

	return passphrase, nil
}

func sendClientKeyHandler(_ *event.Message) error {
	if currState.KeyVersion == currState.SharedKeyVersion {
		logrus.Debug("key version already shared, skipping send")
		return nil
	}

	logrus.Debug("sending client key to server")
	pubKey, err := crypto.ArmorPublicKey()
	if err != nil {
		logrus.WithError(err).Error("unable to armor public key")
		return nil
	}

	if err := sendClientKey(context.Background(), pubKey); err != nil {
		logrus.WithError(err).Error("unable to send public key to hub")
	} else {
		currState.SharedKeyVersion = currState.KeyVersion
	}

	return nil
}
