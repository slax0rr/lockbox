package daemon

import (
	"io/ioutil"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/pkg/event"
	"gitlab.com/slax0rr/lockbox/pkg/pwdgen"
)

func generatePassword(
	name string,
	length int32,
	noSymbols, overwrite bool,
) (*lockbox.Password, error) {
	if length == 0 {
		length = config.GetInt32("password.length")
	}

	genOpts := []pwdgen.PasswordOpt{
		pwdgen.WithPasswordLength(int(length)),
	}

	if noSymbols {
		genOpts = append(genOpts, pwdgen.WithAlphanumPool(),
			pwdgen.WithPasswordRequirements([]pwdgen.PasswordRequirement{
				pwdgen.ContainsUpperCaseChars(1),
				pwdgen.ContainsLowerCaseChars(1),
				pwdgen.ContainsNumbers(1),
			}))
	}

	pass := lockbox.NewPassword(name)
	if !overwrite && pass.Exists() {
		logrus.Info("password exists, refusing generation")
		return nil, lockbox.ErrPasswordExists
	}

	err := pwdgen.GeneratePassword(pass, genOpts...)
	if err != nil {
		logrus.WithError(err).Error("unable to generate random password")
		return nil, err
	}

	event.Dispatch(ev_SECRET_GENERATED, pass)

	return pass, nil
}

func getPassword(name string) (*lockbox.Password, error) {
	p := lockbox.NewPassword(name)
	if !p.Exists() {
		logrus.WithField("password_name", name).
			Debug("requested password does not exist")
		return nil, errors.New("requested password does not exist")
	}

	err := p.ReadFromFile()
	if err != nil {
		logrus.WithError(err).Error("unable to read password file")
		return nil, errors.New("unable to read password file")
	}

	err = p.Decrypt()
	if err != nil {
		logrus.WithError(err).Error("unable to decrypt password")
		return nil, errors.New("unable to decrypt password")
	}

	return p, nil
}

// readAllPasswords from the password_path directory and its child directories,
// returning them as a slice.
func readAllPasswords(subDir string) ([]lockbox.Password, error) {
	files, err := ioutil.ReadDir(
		filepath.Join(config.GetPath("general.password_path"), subDir),
	)
	if err != nil {
		return nil, err
	}

	passwords := make([]lockbox.Password, 0, len(files))
	for _, f := range files {
		if f.IsDir() {
			p, err := readAllPasswords(filepath.Join(subDir, f.Name()))
			if err != nil {
				return nil, err
			}
			passwords = append(passwords, p...)
			continue
		}

		p := lockbox.NewPassword(filepath.Join(subDir, f.Name()))
		err := p.ReadFromFile()
		if err != nil {
			pterm.FgRed.Println("FAIL")
			logrus.WithError(err).Error("unable to read password from file")
			return nil, err
		}

		err = p.Decrypt()
		if err != nil {
			pterm.FgRed.Println("FAIL")
			logrus.WithError(err).Error("unable to decrypt password")
			return nil, errors.New("unable to decrypt password")
		}

		passwords = append(passwords, *p)
	}

	return passwords, nil
}

func setPassword(name, secret string) error {
	pass := lockbox.NewPassword(name)
	_, err := pass.Write([]byte(secret))
	if err != nil {
		return errors.Wrap(err, "writing secret")
	}

	err = pass.Close()
	if err != nil {
		return errors.Wrap(err, "encrypting and writing password")
	}

	return nil
}
