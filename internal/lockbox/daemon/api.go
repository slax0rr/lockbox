package daemon

import (
	"context"
	"fmt"
	"os"
	"runtime"

	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/apiclient"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/apiclient/account"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/apiclient/authentication"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/models"
	"gitlab.com/slax0rr/lockbox/pkg/event"
)

var apiClient *apiclient.Lockbox

type serverConn struct {
	host  string
	port  int64
	email string
	pass  string
}

const (
	defaultServerPort int64 = 9378
)

func openAPIConn() (err error) {
	if !config.GetBool("server.use") {
		return
	}
	apiClient, err = initClient(config.GetString("server.host"))
	return
}

func initClient(hostname string) (*apiclient.Lockbox, error) {
	cl := apiclient.NewHTTPClientWithConfig(nil,
		apiclient.DefaultTransportConfig().WithHost(hostname))

	_, err := cl.Health.GetHealth(nil)
	if err != nil {
		logrus.WithError(err).Error("api server unhealthy")
		return nil, err
	}

	logrus.WithField("hostname", hostname).Debug("api server is healthy")

	return cl, nil
}

// openConnection makes a new connection to the server and authenticates with
// the provided credentials. If the register flag is set to true a request to
// create the account on the server is made.
func openConnection(ctx context.Context, conn serverConn, register bool) error {
	if conn.port == 0 {
		conn.port = defaultServerPort
	}

	cl, err := initClient(fmt.Sprintf("%s:%d", conn.host, conn.port))
	if err != nil {
		return errors.Wrap(err, "api client init failed")
	}

	if conn.pass == "" {
		bs, err := readKeyPassphrase("Enter password for your lockbox account")
		if err != nil {
			logrus.WithError(err).Error("reading account password failed")
			return errors.Wrap(err, "unable to read lockbox account password")
		}
		conn.pass = string(bs)
	}

	device := models.Device{}
	if id := config.GetString("device.id"); id != "" {
		name := config.GetString("device.name")
		device.ID = &id
		device.Name = &name
	} else {
		id := uuid.Nil.String()
		name, err := os.Hostname()
		if err != nil {
			logrus.WithError(err).Error("failed obtaining device hostname")
			return errors.Wrap(err, "failed obtaining device hostname")
		}
		device.ID = &id
		device.Name = &name
	}
	deviceType := runtime.GOOS
	device.Type = &deviceType

	if register {
		params := account.NewPostAccountParams()
		params.SetContext(ctx)
		params.SetBody(&models.Credentials{
			Email:    &conn.email,
			Password: (*strfmt.Password)(&conn.pass),
		})

		created, err := cl.Account.PostAccount(params)
		if err != nil {
			logrus.WithError(err).Info("account registration failed")
			return lockbox.ErrRegisterFailed
		}
		logrus.WithField("created", created).Debug("account created")
	}

	params := authentication.NewAuthUserParams()
	params.SetContext(ctx)
	params.SetBody(&models.Auth{
		Credentials: &models.Credentials{
			Email:    &conn.email,
			Password: (*strfmt.Password)(&conn.pass),
		},
		Device: &device,
	})

	resp, err := cl.Authentication.AuthUser(params)
	if err != nil {
		logrus.WithError(err).Info("account authentication failed")
		return lockbox.ErrAuthFailed
	}
	logrus.WithField("resp", resp).Debug("auth user call complete")

	if resp.Payload == nil {
		logrus.Info("authentication response empty")
		return errors.New("authentication failed - empty response from server")
	}

	config.Set("server.token", *resp.Payload.Token.Token)
	config.Set("device.id", *resp.Payload.Device.ID)
	config.Set("device.name", *resp.Payload.Device.Name)
	if err := config.UpdateConfig(); err != nil {
		logrus.WithError(err).Error("unable to update server config")
		return errors.Wrap(err, "server config update failed")
	}

	currState.hubConnection(true, *resp.Payload.Token.Token)

	event.Dispatch(ev_NEW_HUB_CONNECTION)

	return nil
}
