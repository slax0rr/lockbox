package daemon

import (
	"io/ioutil"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/test"
)

func TestPinentryRead(t *testing.T) {
	Convey("Given an Pinentry instance", t, func() {
		p := &Pinentry{}

		Convey("reading of short pin succeeds", func() {
			pass := []byte("foobar")
			p.res = pass
			pin, err := ioutil.ReadAll(p)
			So(err, ShouldBeNil)
			So(pin, test.ShouldDeepEqual, pass)
			So(len(pin), ShouldEqual, len(pass))
		})

		Convey("reading of long pin succeeds", func() {
			pass := []byte("foobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobar")
			p.res = pass
			pin, err := ioutil.ReadAll(p)
			So(err, ShouldBeNil)
			So(pin, test.ShouldDeepEqual, pass)
			So(len(pin), ShouldEqual, len(pass))
		})
	})
}
