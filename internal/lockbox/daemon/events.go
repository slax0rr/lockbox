package daemon

import "gitlab.com/slax0rr/lockbox/pkg/event"

const (
	ev_DAEMON_INITED      string = "DAEMON_INITED"
	ev_NEW_HUB_CONNECTION string = "NEW_HUB_CONNECTION"
	ev_KEYS_INITED        string = "KEYS_INITED"
	ev_SECRET_GENERATED   string = "SECRET_GENERATED"
)

func init() {
	event.Create(ev_DAEMON_INITED)
	event.Create(ev_NEW_HUB_CONNECTION)
	event.Create(ev_KEYS_INITED)
	event.Create(ev_SECRET_GENERATED)
}
