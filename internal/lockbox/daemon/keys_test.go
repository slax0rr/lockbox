package daemon_test

import (
	"bytes"
	"context"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/daemon"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
)

func TestGenerateKeys(t *testing.T) {
	oldPath := config.GetString("general.path")
	defer func() {
		config.Set("general.key_path", oldPath)
	}()

	Convey("Generate new keys", t, func() {
		passphrase := bytes.NewBufferString("foobar")
		daemon.SetSecretReader(&testSecretReader{passphrase})

		_, err := cl.GenerateKeys(context.Background(), &pb.Identity{
			Name:  "test name",
			Email: "test@mail",
		})
		So(err, ShouldBeNil)
		So(fs.FileExists(config.GetPath("general.key_path")+"primarykey"), ShouldBeNil)
	})
}

func TestKeyRenewal(t *testing.T) {
	Convey("Create password for key renewal", t, func() {
		oldPath := config.GetString("general.password_path")
		defer config.Set("general.password_path", oldPath)
		config.Set("general.password_path", config.GetPath("general.path")+"renew-data/")

		passphrase := bytes.NewBufferString("foobar")
		daemon.SetSecretReader(&testSecretReader{passphrase})

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		_, err := cl.UnlockPrivateKey(ctx, &empty.Empty{})
		So(err, ShouldBeNil)

		length := int32(10)
		passName := "pass-renew"
		genPass, err := cl.GeneratePassword(context.Background(), &pb.GenerateCommand{
			Pass: &pb.Password{
				Name:   passName,
				Length: length,
			},
		})
		So(err, ShouldBeNil)

		Convey("renew encryption keys", func() {
			passphrase := bytes.NewBufferString("newtestpass")
			daemon.SetSecretReader(&testSecretReader{passphrase})

			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			_, err := cl.RenewKeys(ctx, &pb.Identity{
				Name:  "test name",
				Email: "test@mail",
			})
			So(err, ShouldBeNil)

			Convey("password can be decrypted", func() {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second)
				defer cancel()

				pass, err := cl.GetPassword(ctx, &pb.Password{
					Name: passName,
				})
				So(err, ShouldBeNil)
				So(pass.GetPass(), ShouldEqual, genPass.GetPass())
			})
		})
	})
}
