package daemon

import (
	"io"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/pkg/pinentry"
)

type SecretReader interface {
	io.Reader
	SetPrompt(string, string)
}

var secretReader SecretReader

func SetSecretReader(sr SecretReader) {
	secretReader = sr
}

type Pinentry struct {
	pe  *pinentry.Pinentry
	arg pinentry.SecretEntryArg
	res []byte
	i   int64 // reader index
}

func NewPinentry(tty string) error {
	pe := pinentry.New("/usr/bin/pinentry", logrus.StandardLogger(), tty)
	err1, err2 := pe.Init()
	if err1 != nil || err2 != nil {
		logrus.WithError(err1).WithField("error2", err2).
			Error("unable to init pinentry")
		return errors.New("failed to initialize secret reader")
	}
	SetSecretReader(&Pinentry{pe: pe, arg: pinentry.SecretEntryArg{}})

	return nil
}

func (p *Pinentry) SetPrompt(prompt, desc string) {
	p.arg.Prompt = prompt
	p.arg.Desc = desc
}

func (p *Pinentry) Read(out []byte) (n int, err error) {
	if err := p.get(); err != nil {
		return 0, err
	}

	if p.i >= int64(len(p.res)) {
		p.res = nil
		return 0, io.EOF
	}

	n = copy(out, p.res[p.i:])
	p.i += int64(n)
	return
}

func (p *Pinentry) get() error {
	if p.res != nil {
		return nil
	}
	res, err := p.pe.Get(p.arg)
	if err != nil {
		return err
	}

	if res.Canceled {
		return errors.New("user canceled")
	}

	p.res = []byte(res.Text)
	p.i = 0

	return nil
}
