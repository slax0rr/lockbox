package daemon

import (
	"context"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"gitlab.com/slax0rr/lockbox/internal/logger"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
	"gitlab.com/slax0rr/lockbox/pkg/event"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var dmn *lockboxDaemon

func Init(cfgFile, tty string) error {
	dmn = &lockboxDaemon{}

	config.Init(cfgFile)
	logger.Init(logger.WithLogFile(config.GetString("daemon.logfile")),
		logger.WithJSONFormatter(true))

	if err := loadState(); err != nil {
		logrus.WithError(err).Warn("state could not be loaded, continuing with empty state")
	}

	// TODO(slax0rr): remove REST API in favour of gRPC
	if err := openAPIConn(); err != nil {
		return err
	}

	if err := dialHub(context.Background()); err != nil {
		return err
	}

	if err := initKeys(); err != nil {
		return err
	}

	if err := NewPinentry(tty); err != nil {
		logrus.WithError(err).Error("failed initializing pinentry")
		return err
	}

	event.Dispatch(ev_DAEMON_INITED)

	return nil
}

func Start() error {
	logrus.WithField("sockfile", config.GetPath("general.sockfile")).
		Debug("starting daemon listener")

	// TODO(slax0rr): handle condition if socket file exists
	lis, err := net.Listen("unix", config.GetPath("general.sockfile"))
	if err != nil {
		logrus.WithError(err).Error("unable to start daemon listener")
		return err
	}

	defer func() {
		if err := fs.Remove(config.GetString("general.sockfile")); err != nil {
			logrus.WithError(err).Error("unable to remove sockfile")
		}
		logrus.Debug("sockfile removed")
	}()

	dmn.srv = grpc.NewServer()
	pb.RegisterLockboxServer(dmn.srv, dmn)
	go func(s *grpc.Server) {
		if err := s.Serve(lis); err != nil {
			logrus.WithError(err).Panic("server ended abruptly")
		}

	}(dmn.srv)

	logrus.Debug("daemon listener started - waiting for signals")
	waitForSignals()
	logrus.Debug("daemon stopped")

	return nil
}

func Teardown(graceful bool) {
	if err := currState.persistState(); err != nil {
		logrus.WithError(err).Error("state persistance failed")
	}

	if err := closeHubConn(); err != nil {
		logrus.WithError(err).Error("closing of hub connection failed")
	}

	if dmn != nil {
		dmn.Stop(graceful)
	}
}

type lockboxDaemon struct {
	pb.UnimplementedLockboxServer

	srv *grpc.Server
}

func (d lockboxDaemon) Stop(graceful bool) {
	if d.srv == nil {
		return
	}

	if graceful {
		d.srv.GracefulStop()
		return
	}

	d.srv.Stop()
}

func (*lockboxDaemon) GetPassword(ctx context.Context,
	pw *pb.Password,
) (*pb.Password, error) {
	p, err := getPassword(pw.Name)
	if err != nil {
		return nil, err
	}
	pw.Pass = p.String()
	return pw, nil
}

func (*lockboxDaemon) GeneratePassword(ctx context.Context,
	cmd *pb.GenerateCommand,
) (*pb.Password, error) {
	var noSymbols, overwrite bool
	for _, o := range cmd.Opts {
		switch o.Val {
		case pb.LockboxOption_NO_SYMBOLS:
			noSymbols = true
		case pb.LockboxOption_OVERWRITE:
			overwrite = true
		}
	}

	pass, err := generatePassword(cmd.Pass.GetName(), cmd.Pass.GetLength(),
		noSymbols, overwrite)
	switch err {
	case nil:

	case lockbox.ErrPasswordExists:
		return nil, status.Error(codes.AlreadyExists, err.Error())

	default:
		return nil, status.Error(codes.Unknown, err.Error())
	}

	cmd.Pass.Pass = pass.String()

	err = pass.Close()
	if err != nil {
		logrus.WithError(err).Error("unable to encrypt and write password")
		return nil, err
	}

	return cmd.Pass, nil
}

func (*lockboxDaemon) PasswordExists(ctx context.Context,
	pw *pb.Password,
) (*wrappers.BoolValue, error) {
	return &wrappers.BoolValue{
		Value: lockbox.NewPassword(pw.Name).Exists(),
	}, nil
}

func (d *lockboxDaemon) CheckSetup(ctx context.Context,
	e *empty.Empty,
) (*wrappers.BoolValue, error) {
	keysExist := keyExists()
	bv := &wrappers.BoolValue{Value: keysExist}

	if keysExist {
		return bv, nil
	}

	return bv, status.Error(codes.FailedPrecondition, lockbox.ErrKeysNotFound.Error())
}

func (*lockboxDaemon) GenerateKeys(ctx context.Context,
	identity *pb.Identity,
) (*empty.Empty, error) {
	return &empty.Empty{}, createKeyPair(identity.GetName(), identity.GetEmail())
}

func (*lockboxDaemon) RenewKeys(ctx context.Context,
	identity *pb.Identity,
) (*empty.Empty, error) {
	return &empty.Empty{}, renewKeys(identity.GetName(), identity.GetEmail())
}

func (*lockboxDaemon) IsPrivateKeyUnlocked(ctx context.Context,
	e *empty.Empty,
) (*wrappers.BoolValue, error) {
	return &wrappers.BoolValue{
		Value: !crypto.PrivateKeyEncrypted(),
	}, nil
}

func (*lockboxDaemon) UnlockPrivateKey(ctx context.Context,
	e *empty.Empty,
) (*empty.Empty, error) {
	return e, unlockPrivateKey()
}

func (*lockboxDaemon) ConnectToServer(ctx context.Context,
	conn *pb.ServerConnection,
) (*empty.Empty, error) {
	err := openConnection(ctx, serverConn{
		host:  conn.Host,
		port:  conn.Port,
		email: conn.Email,
		pass:  conn.Password,
	}, conn.Register)
	switch err {
	case nil:
		return &empty.Empty{}, nil

	case lockbox.ErrAuthFailed:
		return &empty.Empty{}, status.Error(codes.Unauthenticated, err.Error())

	default:
		return &empty.Empty{}, status.Error(codes.Internal, err.Error())
	}
}

func waitForSignals() {
	sig := make(chan os.Signal, 1024)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT)
	for {
		select {
		case s := <-sig:
			switch s {
			case syscall.SIGHUP:
				Teardown(true)
				return

			case syscall.SIGTERM, syscall.SIGINT:
				Teardown(false)
				return

			default:
				logrus.WithField("signal", s).Debug("unhandled signal caught")
			}
		}
	}
}
