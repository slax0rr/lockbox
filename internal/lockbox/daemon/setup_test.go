package daemon_test

import (
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/daemon"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
	"google.golang.org/grpc"
)

const (
	generalPath  = "/tmp/lockbox-test-daemon/"
	keyPath      = "/tmp/lockbox-test-daemon/keys/"
	passwordPath = "/tmp/lockbox-test-daemon/data/"
	sockfile     = "/tmp/lockbox-test-daemon/lockbox-test.sock"
	cfgFile      = "/tmp/lockbox-test-daemon/lockbox-test.toml"
)

var (
	cl         pb.LockboxClient
	clientConn *grpc.ClientConn
)

func TestMain(m *testing.M) {
	if fs.FileExists(generalPath) == nil {
		if err := fs.RemoveAll(generalPath); err != nil {
			panic(err)
		}
	}

	err := fs.CreateDir(passwordPath, 0700, true)
	if err != nil {
		fmt.Printf("unable to create test password dir: %s\n", err)
		os.Exit(1)
	}

	err = fs.CreateDir(keyPath, 0700, true)
	if err != nil {
		fmt.Printf("unable to create test key dir: %s\n", err)
		os.Exit(1)
	}

	config.Init(cfgFile)
	config.Set("general.path", generalPath)
	config.Set("general.password_path", passwordPath)
	config.Set("general.key_path", keyPath)
	config.Set("general.sockfile", sockfile)

	err = daemon.Init(cfgFile, "")
	if err != nil {
		fmt.Printf("unable to init daemon: %s\n", err)
		os.Exit(1)
	}

	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.DebugLevel)
	crypto.SetRSABits(1024) // good enough for test
	_, err = crypto.GeneratePrimaryKey("foo bar", "foo@bar.com", "foobar")
	if err != nil {
		fmt.Printf("unable to generate primary key: %s\n", err)
		os.Exit(1)
	}

	go func() {
		err := daemon.Start()
		if err != nil {
			fmt.Printf("unable to start daemon: %s\n", err)
			os.Exit(1)
		}
	}()

	err = dialDaemon()
	if err != nil {
		fmt.Printf("unable to dial daemon: %s\n", err)
		os.Exit(1)
	}
	defer clientConn.Close()

	ret := m.Run()

	err = os.RemoveAll(generalPath)
	if err != nil {
		panic(err)
	}

	os.Exit(ret)
}

func dialDaemon() (err error) {
	clientConn, err = grpc.Dial("unix://"+config.GetString("general.sockfile"),
		grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(2*time.Second))
	if err != nil {
		return
	}
	cl = pb.NewLockboxClient(clientConn)

	return
}

type testSecretReader struct {
	reader io.Reader
}

func (*testSecretReader) SetPrompt(prompt, desc string) {}

func (sr *testSecretReader) Read(p []byte) (int, error) {
	return sr.reader.Read(p)
}
