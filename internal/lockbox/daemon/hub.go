package daemon

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"gitlab.com/slax0rr/lockbox/pkg/event"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const (
	defaultHubPort int64 = 9369
)

var (
	cl       pb.LockboxHubClient
	hubConn  *grpc.ClientConn
	pipeline pb.LockboxHub_PipelineClient
)

func init() {
	event.RegisterHandler(ev_DAEMON_INITED, openPipelineHandler)
	event.RegisterHandler(ev_NEW_HUB_CONNECTION, openPipelineHandler)
	event.RegisterHandler(ev_SECRET_GENERATED, sendSecretHandler)
}

func dialHub(ctx context.Context) (err error) {
	// TODO(slax0rr): remove and use host directly once REST API is removed in
	// favour of gRPC
	serverHost := strings.Split(config.GetString("server.host"), ":")[0]

	// TODO(slax0rr): do not use insecure!
	hubConn, err = grpc.DialContext(ctx, fmt.Sprintf("%s:%d", serverHost, defaultHubPort), grpc.WithInsecure(),
		grpc.WithTimeout(10*time.Second))
	if err != nil {
		logrus.WithError(err).Error("unable to dial lockbox hub")
		return
	}

	cl = pb.NewLockboxHubClient(hubConn)

	return
}

func closeHubConn() error {
	if hubConn == nil {
		return nil
	}

	return hubConn.Close()
}

func sendClientKey(ctx context.Context, pubKey []byte) error {
	header := metadata.New(map[string]string{"authorization": fmt.Sprintf("bearer %s", currState.HubToken)})
	ctx = metadata.NewOutgoingContext(ctx, header)

	_, err := cl.SendClientKey(ctx, &pb.ClientKey{
		PublicKey: string(pubKey),
	})
	if err != nil {
		return err
	}

	logrus.WithField("public_key", pubKey).Debug("public key sent to hub")

	return nil
}

func openPipeline(ctx context.Context) (err error) {
	if !currState.HubConnected {
		return
	}

	header := metadata.New(map[string]string{"authorization": fmt.Sprintf("bearer %s", currState.HubToken)})
	ctx = metadata.NewOutgoingContext(ctx, header)

	pipeline, err = cl.Pipeline(ctx)
	if err != nil {
		return
	}

	c := make(chan *pb.Event, 10)

	go func(ctx context.Context, c chan *pb.Event) {
		for {
			select {
			case ev := <-c:
				switch ev.Type {
				case pb.Event_PUBKEY_UPDATE:
					if err := handleDeviceKey(ctx, ev.GetDevice(), ev.GetDeviceClientKey()); err != nil {
						logrus.WithError(err).Error("failed adding device key")
					}

				case pb.Event_SECRET_UPDATE:
					logrus.WithField("secret", ev.GetSecret().GetName()).
						Debug("received new secret")

					sec := ev.GetSecret()
					if err := setPassword(sec.GetName(), sec.GetSecret()); err != nil {
						logrus.WithError(err).Error("unable to store received secret")
					}

				default:
					logrus.WithField("event", ev.Type).Warn("received unknown event")
				}

			case <-ctx.Done():
				logrus.Info("stopping event listener loop")
				return
			}
		}
	}(ctx, c)

	for {
		var ev *pb.Event
		ev, err = pipeline.Recv()
		if err == io.EOF {
			logrus.Info("pipeline closed by hub")
			return nil
		} else if err != nil {
			return
		}

		select {
		case c <- ev:

		case <-ctx.Done():
			logrus.Info("stopping pipeline receive loop")
			return ctx.Err()
		}
	}
}

func handleDeviceKey(ctx context.Context, device *pb.Device, clientKey *pb.ClientKey) error {
	logrus.WithField("device", device).
		WithField("pubkey", clientKey).
		Debug("storing device key")

	return storeDeviceKey(device.GetId(), []byte(clientKey.GetPublicKey()))
}

func openPipelineHandler(_ *event.Message) error {
	go func() {
		err := openPipeline(context.Background())
		if err != nil {
			panic(err)
		}
	}()
	return nil
}

func sendSecretHandler(msg *event.Message) error {
	if !currState.HubConnected {
		return nil
	}
	logrus.Debug("sending secret")

	if msg == nil || len(msg.Data) == 0 {
		logrus.Error("send secret: event does not contain a secret")
		return nil
	}

	sec, ok := msg.Data[0].(*lockbox.Password)
	if !ok {
		logrus.Error("send secret: secret is not of correct type")
		return nil
	}

	go func() {
		ev := &pb.Event{
			Type: pb.Event_SECRET_UPDATE,
			Secret: &pb.Secret{
				Name:   sec.Name(),
				Secret: sec.String(),
			},
		}

		err := pipeline.Send(ev)
		if err != nil {
			logrus.WithError(err).Error("sending secret to hub")
			return
		}
	}()

	return nil
}
