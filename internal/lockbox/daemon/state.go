package daemon

import (
	"crypto/sha512"
	"encoding/gob"
	"path"

	"github.com/pkg/errors"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
)

var currState state

type state struct {
	KeyVersion       [sha512.Size256]byte
	SharedKeyVersion [sha512.Size256]byte
	HubConnected     bool
	HubToken         string
}

func (s *state) setCurrentKeyVersion(key []byte) bool {
	newVersion := sha512.Sum512_256(key)

	if newVersion != s.KeyVersion {
		s.KeyVersion = newVersion
		return true
	}

	return false
}

func (s *state) hubConnection(connected bool, token string) {
	s.HubConnected = connected
	if token != "" {
		s.HubToken = token
	}
}

func (s state) persistState() error {
	stateFile := config.GetPath("daemon.statefile")
	if stateFile == "" {
		stateFile = path.Join(config.GetPath("general.path"), "state")
		config.Set("daemon.statefile", stateFile)
		if err := config.UpdateConfig(); err != nil {
			return errors.Wrap(err, "update config after setting statefile")
		}

		if stateFile == "" {
			return errors.New("unable to locate state file")
		}
	}

	f, err := fs.CreateFile(stateFile)
	if err != nil {
		return errors.Wrap(err, "create state file")
	}
	defer f.Close()

	return gob.NewEncoder(f).Encode(s)
}

func loadState() error {
	stateFile := config.GetPath("daemon.statefile")

	f, err := fs.OpenFile(stateFile)
	if err != nil {
		return errors.Wrap(err, "open state file for reading")
	}

	err = gob.NewDecoder(f).Decode(&currState)
	if err != nil {
		return errors.Wrap(err, "decode state")
	}

	return nil
}
