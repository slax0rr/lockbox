package models

import (
	"time"

	"gorm.io/gorm"
)

type Account struct {
	Email     string `gorm:"primaryKey"`
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
