package models

import (
	"time"

	"gorm.io/gorm"
)

type DeviceDB struct {
	UUID         string `gorm:"primaryKey"`
	AccountEmail string
	Account      *Account `gorm:"foreignKey:AccountEmail"`
	Type         string
	Name         string
	PublicKey    string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    gorm.DeletedAt `gorm:"index"`
}

func (DeviceDB) TableName() string {
	return "devices"
}
