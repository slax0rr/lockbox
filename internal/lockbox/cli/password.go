package cli

import (
	"context"
	"errors"

	"github.com/atotto/clipboard"
	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
)

// GetPassword reads the password from the filesystem, decrypts it and writes it
// to output or the clipboard, depending on the opts.
// TODO: handle non existing passwords nicely
func GetPassword(name string, opts ...LockboxOpt) error {
	options := getOptions(opts...)

	if err := UnlockPrivateKey(); err != nil {
		return err
	}

	p := lockbox.NewPassword(name)
	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		pw, err := cl.GetPassword(ctx, &pb.Password{Name: name})
		if pw != nil {
			_, err = p.Write([]byte(pw.Pass))
		}
		return err
	})
	if err != nil {
		logrus.WithError(err).Error("could not obtain password")
		return errors.New("could not obtain password")
	}

	if options.clip {
		clipboard.WriteAll(p.String())
	} else {
		// clip flag not passed, printing to stdout
		pterm.Println(p.String())
	}

	return nil
}
