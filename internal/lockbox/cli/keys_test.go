package cli_test

import (
	"bytes"
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func TestUnlockPrivateKey(t *testing.T) {
	Convey("Check if the private key is unlocked", t, func() {
		mockClient := getMockClient()

		Convey("and the key has not yet been unlocked", func() {
			mockClient.On("IsPrivateKeyUnlocked", mock.Anything,
				mock.AnythingOfType("*emptypb.Empty")).
				Return(&wrapperspb.BoolValue{Value: false}, nil)

			mockClient.On("UnlockPrivateKey", mock.Anything,
				mock.AnythingOfType("*emptypb.Empty")).
				Return(&emptypb.Empty{}, nil)

			err := cli.UnlockPrivateKey()
			So(err, ShouldBeNil)

			mockClient.AssertExpectations(t)
		})

		Convey("and the key has already been unlocked", func() {
			mockClient.On("IsPrivateKeyUnlocked", mock.Anything,
				mock.AnythingOfType("*emptypb.Empty")).
				Return(&wrapperspb.BoolValue{Value: true}, nil)

			err := cli.UnlockPrivateKey()
			So(err, ShouldBeNil)

			mockClient.AssertExpectations(t)
		})
	})
}

func TestRenewKeys(t *testing.T) {
	Convey("A key renewal command is executed", t, func() {
		mockClient := getMockClient()

		name := "foo bar"
		email := "foo@bar"

		cli.SetInputScanner(bytes.NewBufferString(fmt.Sprintf("%s\n%s\n", name, email)))

		mockClient.On("RenewKeys", mock.Anything, mock.MatchedBy(func(ident *pb.Identity) bool {
			return ident.GetName() == name && ident.GetEmail() == email
		})).Return(&emptypb.Empty{}, nil)

		err := cli.RenewKeys()
		So(err, ShouldBeNil)

		mockClient.AssertExpectations(t)
	})
}
