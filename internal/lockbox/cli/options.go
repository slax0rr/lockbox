package cli

type lockboxOpts struct {
	// noSymbols forces password to be generated using only Alpha-Numeric
	//characters
	noSymbols bool

	// clip will copy the newly generated password into the clipboard.
	clip bool

	// force overwrite of an existing password if it already exists.
	force bool

	// length of the generated password.
	length int

	// name of the generated password.
	name string

	// hostname of the lockbox server
	hostname string

	// port of the lockbox server
	port int64

	// email for authentication against the lockbox server
	email string

	// password for authentication against the lockbox server
	password string
}

type LockboxOpt func(*lockboxOpts)

func WithNoSymbols(noSymbols bool) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.noSymbols = noSymbols
	}
}

func WithClip(clip bool) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.clip = clip
	}
}

func WithForce(force bool) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.force = force
	}
}

func WithLength(length int) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.length = length
	}
}

func WithServerHost(host string) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.hostname = host
	}
}

func WithServerPort(port int64) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.port = port
	}
}

func WithServerEmail(user string) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.email = user
	}
}

func WithServerPassword(pass string) LockboxOpt {
	return func(opts *lockboxOpts) {
		opts.password = pass
	}
}

func getOptions(opts ...LockboxOpt) lockboxOpts {
	options := lockboxOpts{}

	for _, opt := range opts {
		opt(&options)
	}

	return options
}

// TODO(slax0rr): converter to and from protobuf options
