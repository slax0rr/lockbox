package cli_test

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/atotto/clipboard"
	"github.com/pterm/pterm"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func TestGenerate(t *testing.T) {
	Convey("Generate a password when", t, func() {
		mockClient := getMockClient()

		Convey("the password does not exist", func() {
			name := "pass"
			length := int32(9)
			pass := "foobarbaz"
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)

			mockClient.On("PasswordExists", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name &&
					pw.GetLength() == length
			})).Return(&wrapperspb.BoolValue{Value: false}, nil)

			mockClient.On("GeneratePassword", mock.Anything, mock.MatchedBy(func(gc *pb.GenerateCommand) bool {
				return gc.GetPass().GetName() == name &&
					gc.GetPass().GetLength() == length &&
					len(gc.GetOpts()) == 0
			})).Return(&pb.Password{Name: name, Length: length, Pass: pass}, nil)

			err := cli.Generate(name, cli.WithLength(int(length)))
			So(err, ShouldBeNil)
			So(buf.String(), ShouldEqual, pass+"\n") // newline character added by pterm when printing to terminal

			mockClient.AssertExpectations(t)
		})

		Convey("a no symbols option is requested", func() {
			name := "pass"
			length := int32(9)
			pass := "foobarbaz"
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)

			mockClient.On("PasswordExists", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name &&
					pw.GetLength() == length
			})).Return(&wrapperspb.BoolValue{Value: false}, nil)

			mockClient.On("GeneratePassword", mock.Anything, mock.MatchedBy(func(gc *pb.GenerateCommand) bool {
				return gc.GetPass().GetName() == name &&
					gc.GetPass().GetLength() == length &&
					len(gc.GetOpts()) == 1 &&
					gc.GetOpts()[0].GetVal() == pb.LockboxOption_NO_SYMBOLS
			})).Return(&pb.Password{Name: name, Length: length, Pass: pass}, nil)

			err := cli.Generate(name, cli.WithLength(int(length)), cli.WithNoSymbols(true))
			So(err, ShouldBeNil)
			So(buf.String(), ShouldEqual, pass+"\n") // newline character added by pterm when printing to terminal

			mockClient.AssertExpectations(t)
		})

		Convey("clip option is requested", func() {
			name := "pass"
			length := int32(9)
			pass := "foobarbaz"
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)
			prevClipboard, err := clipboard.ReadAll()
			if err != nil {
				t.Logf("unable to read clipboard: %s", err)
				SkipConvey(fmt.Sprintf("Skipping clip test, clipboard can not be accessed: %v", err), func() {})
			}
			defer clipboard.WriteAll(prevClipboard)

			mockClient.On("PasswordExists", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name &&
					pw.GetLength() == length
			})).Return(&wrapperspb.BoolValue{Value: false}, nil)

			mockClient.On("GeneratePassword", mock.Anything, mock.MatchedBy(func(gc *pb.GenerateCommand) bool {
				return gc.GetPass().GetName() == name &&
					gc.GetPass().GetLength() == length &&
					len(gc.GetOpts()) == 0
			})).Return(&pb.Password{Name: name, Length: length, Pass: pass}, nil)

			err = cli.Generate(name, cli.WithLength(int(length)), cli.WithClip(true))
			So(err, ShouldBeNil)
			So(buf.Len(), ShouldEqual, 0)

			clipValue, err := clipboard.ReadAll()
			So(err, ShouldBeNil)
			So(clipValue, ShouldEqual, pass)

			mockClient.AssertExpectations(t)
		})

		Convey("the password exists", func() {
			name := "pass"
			length := int32(9)

			mockClient.On("PasswordExists", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name &&
					pw.GetLength() == length
			})).Return(&wrapperspb.BoolValue{Value: true}, nil)

			Convey("and overwrite is skipped", func() {
				cli.SetInputScanner(bytes.NewBufferString("n\n"))

				err := cli.Generate(name, cli.WithLength(int(length)))
				So(err, ShouldBeNil)

				mockClient.AssertExpectations(t)
			})

			Convey("and overwrite is requested", func() {
				pass := "foobarbaz"
				buf := new(bytes.Buffer)
				pterm.SetDefaultOutput(buf)
				cli.SetInputScanner(bytes.NewBufferString("y\n"))

				mockClient.On("GeneratePassword", mock.Anything, mock.MatchedBy(func(gc *pb.GenerateCommand) bool {
					return gc.GetPass().GetName() == name &&
						gc.GetPass().GetLength() == length &&
						len(gc.GetOpts()) == 1 &&
						gc.GetOpts()[0].GetVal() == pb.LockboxOption_OVERWRITE
				})).Return(&pb.Password{Name: name, Length: length, Pass: pass}, nil)

				err := cli.Generate(name, cli.WithLength(int(length)))
				So(err, ShouldBeNil)
				// password prompt is picked up by output buffer, prepend it
				// newline character added by pterm when printing to terminal
				So(buf.String(), ShouldEqual, fmt.Sprintf("Password `%s` exists. Overwrite? (y/n): %s\n", name, pass))

				mockClient.AssertExpectations(t)
			})
		})

		Convey("the password exists and force option is used", func() {
			name := "pass"
			length := int32(9)
			pass := "foobarbaz"
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)

			mockClient.On("GeneratePassword", mock.Anything, mock.MatchedBy(func(gc *pb.GenerateCommand) bool {
				return gc.GetPass().GetName() == name &&
					gc.GetPass().GetLength() == length &&
					len(gc.GetOpts()) == 0
			})).Return(&pb.Password{Name: name, Length: length, Pass: pass}, nil)

			err := cli.Generate(name, cli.WithLength(int(length)), cli.WithForce(true))
			So(err, ShouldBeNil)
			So(buf.String(), ShouldEqual, pass+"\n") // newline character added by pterm when printing to terminal

			mockClient.AssertExpectations(t)
		})
	})
}
