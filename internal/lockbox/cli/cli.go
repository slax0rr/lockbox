package cli

import (
	"bufio"
	"context"
	"io"
	"os"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"gitlab.com/slax0rr/lockbox/internal/logger"
)

var scanner *bufio.Scanner

func init() {
	SetInputScanner(os.Stdin)
}

func readInput(dflt string) string {
	if !scanner.Scan() {
		pterm.FgRed.Println("Uh-oh! Could not read your input.")
		logrus.Fatal("could not read stdin")
	}
	scanned := scanner.Text()
	if scanned == "" {
		return dflt
	}

	return scanned
}

// SetInputScanner sets a Reader as a scanner to read user input.
func SetInputScanner(r io.Reader) {
	scanner = bufio.NewScanner(r)
}

// Init creates the config file if it does not exist, checks the lockbox
// path is writable, and that the encryption keys are set up.
func Init(cfgFile string) error {
	config.Init(cfgFile)
	logger.Init(logger.WithLogFile(config.GetString("client.logfile")),
		logger.WithJSONFormatter(true))

	if err := dialDaemon(); err != nil {
		logrus.WithError(err).Error("unable to dial daemon")
		return err
	}

	var ok bool
	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		bv, err := cl.CheckSetup(ctx, &empty.Empty{})
		if bv != nil {
			ok = bv.Value
		}
		return err
	})
	if err != nil && !isStatusError(err, lockbox.ErrKeysNotFound) {
		logrus.WithError(err).Error("error checking daemon setup")
		return err
	}

	if !ok || isStatusError(err, lockbox.ErrKeysNotFound) {
		err := createKeyPair()
		if err != nil {
			logrus.WithError(err).Error("unable to create key pair")
			return err
		}
	}

	return nil
}
