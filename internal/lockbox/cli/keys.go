package cli

import (
	"context"
	"errors"
	"time"

	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/protobuf/types/known/emptypb"
)

// UnlockPrivateKey checks if the daemon has the key already unlocked and
// immediatelly returns if it does. If the key is not unlocked it initiates the
// request to unlock the key.
func UnlockPrivateKey() error {
	var decrypted bool
	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		bv, err := cl.IsPrivateKeyUnlocked(ctx, &emptypb.Empty{})
		if bv != nil {
			decrypted = bv.Value
		}
		return err
	})
	if err != nil {
		logrus.WithError(err).Error("private key encryption status check failed")
		return errors.New("unable to check if private key is unlocked")
	}

	if decrypted {
		return nil
	}

	err = rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		_, err = cl.UnlockPrivateKey(ctx, &emptypb.Empty{})
		return err
	}, withTimeout(1*time.Minute))
	if err != nil {
		logrus.WithError(err).Error("could not unlock private key")
		return errors.New("could not unlock private key - is your passphrase correct?")
	}

	return nil
}

// RenewKeys generates a new key pair and re-encrypts all existing passwords
// with the new keys.
// The new keys then overwrite the old ones.
func RenewKeys() error {
	name, email := readKeyIdentity()

	pterm.Print("Starting key renewal...")

	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		_, err := cl.RenewKeys(ctx, &pb.Identity{
			Name:  name,
			Email: email,
		})
		return err
	}, withTimeout(5*time.Minute))
	if err != nil {
		logrus.WithError(err).Error("unable to renew your private key")
		pterm.FgRed.Println("FAIL")
		return err
	}

	pterm.FgGreen.Println("OK")

	return nil
}

func createKeyPair() error {
	pterm.Println("Encryption key not found, creating new one.")

	name, email := readKeyIdentity()

	pterm.Print("\nGenerating new keypair...")

	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		_, err := cl.GenerateKeys(ctx, &pb.Identity{
			Name:  name,
			Email: email,
		})
		return err
	}, withTimeout(1*time.Minute))
	if err != nil {
		logrus.WithError(err).Error("unable to generate key pair")
		pterm.FgRed.Println("FAIL")
		return err
	}

	pterm.FgGreen.Println("OK")

	return nil
}

func readKeyIdentity() (name string, email string) {
	pterm.Printf("Your name (%s): ", config.GetString("key.identity.name"))
	name = readInput(config.GetString("key.identity.name"))

	pterm.Printf("Your email address (%s): ", config.GetString("key.identity.email"))
	email = readInput(config.GetString("key.identity.email"))

	config.Set("key.identity.name", name)
	config.Set("key.identity.email", email)

	if err := config.UpdateConfig(); err != nil {
		logrus.WithError(err).Warning("unable to update config with new key identity")
	}

	return
}
