package cli

import (
	"context"
	"errors"

	"github.com/atotto/clipboard"
	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
)

func Generate(name string, opts ...LockboxOpt) error {
	options := getOptions(opts...)
	options.name = name

	if options.length == 0 {
		options.length = config.GetInt("generate.length")
	}

	pass := &pb.Password{Name: options.name, Length: int32(options.length)}
	lbOpts := []*pb.LockboxOption{}

	if !options.force {
		var exists bool
		err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
			bv, err := cl.PasswordExists(ctx, pass)
			if err != nil {
				return err
			}
			exists = bv.Value
			return nil
		})
		if err != nil {
			logrus.WithError(err).Error("password exists check failed")
			return errors.New("unable to check if password exists")
		}

		if exists {
			pterm.Printf("Password `%s` exists. Overwrite? (y/n): ", options.name)
			if readInput("") != "y" {
				return nil
			}

			lbOpts = append(lbOpts, &pb.LockboxOption{Val: pb.LockboxOption_OVERWRITE})
		}
	}

	if options.noSymbols {
		lbOpts = append(lbOpts, &pb.LockboxOption{Val: pb.LockboxOption_NO_SYMBOLS})
	}

	err := rpc(func(ctx context.Context, cl pb.LockboxClient) (err error) {
		pass, err = cl.GeneratePassword(ctx, &pb.GenerateCommand{
			Pass: pass,
			Opts: lbOpts,
		})
		return
	})
	if err != nil {
		logrus.WithError(err).Error("password generation failed")
		return errors.New("unable to generate password")
	}

	if options.clip {
		clipboard.WriteAll(pass.GetPass())
	} else {
		// clip flag not passed, printing to stdout
		pterm.Println(pass.GetPass())
	}

	return nil
}
