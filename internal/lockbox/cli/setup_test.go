package cli_test

import (
	"os"
	"testing"

	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/fs"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
	"gitlab.com/slax0rr/lockbox/pkg/crypto"
	"gitlab.com/slax0rr/lockbox/test/mock"
)

const (
	generalPath  = "/tmp/lockbox-test-cli/"
	passwordPath = "/tmp/lockbox-test-cli/data/"
)

func TestMain(m *testing.M) {
	err := fs.CreateDir("/tmp/lockbox-test/data/", 0700, true)
	if err != nil {
		panic(err)
	}

	config.Set("general.path", generalPath)
	config.Set("general.password_path", passwordPath)
	crypto.SetRSABits(1024) // good enough for test
	_, err = crypto.GeneratePrimaryKey("foo bar", "foo@bar.com", "foobar")
	if err != nil {
		panic(err)
	}

	ret := m.Run()

	err = os.RemoveAll(generalPath)
	if err != nil {
		panic(err)
	}

	os.Exit(ret)
}

func getMockClient() *mock.LockboxClient {
	mockClient := &mock.LockboxClient{}
	cli.SetGRPCClient(mockClient)
	return mockClient
}
