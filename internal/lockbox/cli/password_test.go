package cli_test

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/atotto/clipboard"
	"github.com/pterm/pterm"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

// TODO: ass test-case for a non-existing password
func TestGetPassword(t *testing.T) {
	Convey("Attempt to retrieve a password", t, func() {
		mockClient := getMockClient()
		name := "pass"
		pass := "foobarbaz"

		mockClient.On("IsPrivateKeyUnlocked", mock.Anything, mock.Anything).
			Return(&wrapperspb.BoolValue{Value: true}, nil)

		Convey("that exists", func() {
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)

			mockClient.On("GetPassword", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name
			})).Return(&pb.Password{Name: name, Pass: pass}, nil)

			err := cli.GetPassword(name)
			So(err, ShouldBeNil)
			So(buf.String(), ShouldEqual, pass+"\n") // newline character added by pterm when printing to terminal

			mockClient.AssertExpectations(t)
		})

		Convey("with clip option set", func() {
			buf := new(bytes.Buffer)
			pterm.SetDefaultOutput(buf)
			prevClipboard, err := clipboard.ReadAll()
			if err != nil {
				SkipConvey(fmt.Sprintf("Skipping clip test, clipboard can not be accessed: %v", err), t, func() {})
			}
			defer clipboard.WriteAll(prevClipboard)

			mockClient.On("GetPassword", mock.Anything, mock.MatchedBy(func(pw *pb.Password) bool {
				return pw.GetName() == name
			})).Return(&pb.Password{Name: name, Pass: pass}, nil)

			err = cli.GetPassword(name, cli.WithClip(true))
			So(err, ShouldBeNil)
			So(buf.Len(), ShouldEqual, 0)

			clipValue, err := clipboard.ReadAll()
			So(err, ShouldBeNil)
			So(clipValue, ShouldEqual, pass)

			mockClient.AssertExpectations(t)
		})
	})
}
