package cli

import (
	"context"
	"time"

	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/config"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

// TODO(slax0rr): make a proper implementation of the client, get rid of the
// "rpc" function and make a GRPC client worthy of the name implementing all
// client functions instead of trying to make some generic call function.

var cl pb.LockboxClient
var clientConn *grpc.ClientConn

// dialDaemon opens a grpc connection to the lockbox daemon and initialises the // LockboxClient.
func dialDaemon() (err error) {
	logrus.WithField("sockfile", config.GetPath("general.sockfile")).
		Debug("opening connection to daemon")
	// TODO(slax0rr): secure!
	clientConn, err = grpc.Dial("unix://"+config.GetPath("general.sockfile"),
		grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(time.Second))
	if err != nil {
		logrus.WithError(err).Error("unable to connect to lockbox daemon")
		pterm.FgRed.Println("Unable to reach the lockbox daemon")
		return
	}
	cl = pb.NewLockboxClient(clientConn)

	return
}

// CloseGRPCConn closes the grpc connection to the daemon.
func CloseGRPCConn() error {
	logrus.Debug("closing connection to daemon")
	return clientConn.Close()
}

// SetGRPCClient can be used to override the client used for RPC calls.
func SetGRPCClient(client pb.LockboxClient) {
	cl = client
}

type call struct {
	timeout time.Duration
}

type rpcOption func(*call)

func withTimeout(d time.Duration) rpcOption {
	return func(c *call) {
		c.timeout = d
	}
}

type callerFunc func(context.Context, pb.LockboxClient) error

func rpc(caller callerFunc, opts ...rpcOption) error {
	c := call{
		// TODO(slax0rr): get timeout from config
		timeout: time.Second,
	}

	for _, opt := range opts {
		opt(&c)
	}

	ctx, cancel := context.WithTimeout(context.Background(), c.timeout)
	defer cancel()

	return caller(ctx, cl)
}

func isStatusError(actual, expected error) bool {
	statusErr := status.Convert(actual)
	if statusErr == nil {
		return false
	}

	return statusErr.Message() == expected.Error()
}
