package cli

import (
	"context"
	"time"

	"github.com/pterm/pterm"
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/pb"
)

// ConnectToServer makes a remote procedure call to the daemon to connect it to
// the lockbox server.
func ConnectToServer(register bool, opts ...LockboxOpt) error {
	options := getOptions(opts...)

	pterm.Printf("Connecting to lockbox server (%s) ... ", options.hostname)

	err := rpc(func(ctx context.Context, cl pb.LockboxClient) error {
		_, err := cl.ConnectToServer(ctx, &pb.ServerConnection{
			Host:     options.hostname,
			Port:     options.port,
			Email:    options.email,
			Password: options.password,
			Register: register,
		})
		return err
	}, withTimeout(1*time.Minute))
	if err != nil {
		logrus.WithError(err).
			Info("authentication rpc call failed")
		pterm.FgRed.Println("FAIL")
		return err
	}

	pterm.FgGreen.Println("OK")

	return nil
}
