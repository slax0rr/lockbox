package lockbox_test

import (
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/internal/lockbox"
)

func TestPasswordExists(t *testing.T) {
	check := func(name string, assertion func(interface{}, ...interface{}) string) func() {
		return func() {
			pass := lockbox.NewPassword(name)
			So(pass.Exists(), assertion)
		}
	}

	Convey("Given a test file which exists on the file system", t, func() {
		testCases := []struct {
			description string
			name        string
			create      bool
			assertion   func(interface{}, ...interface{}) string
		}{
			{
				"Which exists on the file system and `password` reports it Exists",
				"exists-pass",
				true,
				ShouldBeTrue,
			},
			{
				"Which does not exist on the file system and `password` reports it does not Exists",
				"not-exists-pass",
				false,
				ShouldBeFalse,
			},
		}

		for _, tc := range testCases {
			if tc.create {
				_, err := os.Create(passwordPath + tc.name)
				So(err, ShouldBeNil)
			}
			Convey(tc.description, check(tc.name, tc.assertion))
		}
	})
}

func TestPasswordWriteToFile(t *testing.T) {
	Convey("Given a password", t, func() {
		name := "pass-write"
		pass := lockbox.NewPassword(name)
		_, err := pass.Write([]byte("foobar"))
		So(err, ShouldBeNil)

		Convey("And attempt to write it unencrypted to file system will fail", func() {
			err := pass.WriteToFile()
			So(err, ShouldNotBeNil)

			_, err = os.Stat(passwordPath + name)
			So(os.IsNotExist(err), ShouldBeTrue)
		})

		Convey("And encrypting it", func() {
			err := pass.Close()
			So(err, ShouldBeNil)

			Convey("And attempt to write to file system will succeed", func() {
				err := pass.WriteToFile()
				So(err, ShouldBeNil)

				_, err = os.Stat(passwordPath + name)
				So(err, ShouldBeNil)
			})
		})
	})
}
