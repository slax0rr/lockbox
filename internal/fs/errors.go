package fs

type FilesystemError string

func (err FilesystemError) Error() string {
	return string(err)
}

const (
	ErrFileNotExists FilesystemError = "file does not exist"
	ErrFileCreate    FilesystemError = "unable to create file"
	ErrDirExists     FilesystemError = "directory exists"
	ErrUnexpected    FilesystemError = "unexpected error occured"
)
