// TODO(slax0rr): add writable check for windows
// +build !windows

package fs

import (
	"errors"
	"os"
	"syscall"

	"github.com/sirupsen/logrus"
)

func IsDirWritable(path string) error {
	finfo, err := os.Stat(path)
	if err != nil {
		logrus.WithError(err).Panic("could not stat directory")
		return err
	}

	if !finfo.IsDir() {
		logrus.Error("specified path is not a directory")
		return errors.New("path is not a directory")
	}

	if finfo.Mode().Perm()&(1<<(uint(7))) == 0 {
		logrus.Error("write permission bit is not set for user")
		return errors.New("directory is not writable")
	}

	var stat syscall.Stat_t
	if err = syscall.Stat(path, &stat); err != nil {
		logrus.WithError(err).Error("unable to get stat")
		return err
	}

	if uint32(os.Geteuid()) != stat.Uid {
		logrus.Error("user does not have permissions to write to path")
		return errors.New("directory is not writable")
	}

	return nil
}
