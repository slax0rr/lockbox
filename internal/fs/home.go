package fs

import homedir "github.com/mitchellh/go-homedir"

func GetHome() (string, error) {
	return homedir.Dir()
}

func MustGetHome() string {
	home, err := GetHome()
	if err != nil {
		panic(err)
	}
	return home
}

// ExpandHome expands the '~' or '$HOME' to the users home directory and returns
// the updated path.
func ExpandHome(path string) (string, error) {
	return homedir.Expand(path)
}
