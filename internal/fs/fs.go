package fs

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

// FileExists checks if the provided file or dir exists on the filesystem.
func FileExists(file string) error {
	_, err := os.Stat(file)
	switch {
	case os.IsNotExist(err):
		return ErrFileNotExists

	case err != nil:
		return errors.Wrap(err, ErrUnexpected.Error())

	default:
		return nil
	}
}

// CreateFile will attempt to create the provided file on the filesystem and
// return its handle.
func CreateFile(file string) (*os.File, error) {
	f, err := os.Create(file)
	if err != nil {
		return nil, errors.Wrap(err, ErrFileCreate.Error())
	}

	return f, nil
}

// CreateFileWithPath creates the full file path, along with all the parent
// directories if they do not exist. Returns the created files handle.
func CreateFileWithPath(filePath string, dirPerm os.FileMode) (*os.File, error) {
	err := CreateDir(filepath.Dir(filePath), dirPerm, true)
	if err != nil && !errors.Is(err, ErrDirExists) {
		return nil, err
	}

	return CreateFile(filePath)
}

// OpenFile opens the file under the filePath for reading.
func OpenFile(filePath string) (io.ReadCloser, error) {
	return os.Open(filePath)
}

// CreateDir will create the directory with the given permissions. If parents is
// set to true all parents are also created with the same permissions.
func CreateDir(dir string, perm os.FileMode, parents bool) (err error) {
	if parents {
		err = os.MkdirAll(dir, perm)
	} else {
		err = os.Mkdir(dir, perm)
		if os.IsExist(err) {
			return ErrDirExists
		}
	}

	return errors.Wrap(err, ErrUnexpected.Error())
}

// Remove removes the named file or (empty) directory.
// If there is an error, it will be of type *os.PathError.
func Remove(path string) error {
	return os.Remove(path)
}

// RemoveAll removes the path and all its children.
func RemoveAll(path string) error {
	return os.RemoveAll(path)
}

// Rename oldpath to newpath.
func Rename(oldpath, newpath string) error {
	return os.Rename(oldpath, newpath)
}

// Walk the path recursively calling fn on each file.
func Walk(path string, fn func(string, os.FileInfo, error) error) error {
	return filepath.Walk(path, fn)
}

// MoveDir recursively copies a directory tree, attempting to preserve
// permissions.
// Source directory must exist, destination directory will be wiped if it
// exists. After copying the source directory is removed.
func MoveDir(src string, dst string) (err error) {
	// Function adapted from Roland Singer <roland.singer@desertbit.com> who is
	// the original author of the `CopyDir` function.
	// https://gist.github.com/r0l1/92462b38df26839a3ca324697c8cba04

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return errors.New("source is not a directory")
	}

	_, err = os.Stat(dst)
	if err != nil && !os.IsNotExist(err) {
		return
	}
	if err == nil {
		err = RemoveAll(dst)
		if err != nil {
			return err
		}
	}

	err = os.MkdirAll(dst, si.Mode())
	if err != nil {
		return
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = MoveDir(srcPath, dstPath)
			if err != nil {
				return
			}

			continue
		}

		// Skip symlinks.
		if entry.Mode()&os.ModeSymlink != 0 {
			continue
		}

		err = Rename(srcPath, dstPath)
		if err != nil {
			return
		}
	}

	return RemoveAll(src)
}
