# lockbox

**lockbox** is a password manager providing simple and secure storage on your
device and sharing between your devices and your team the way you want. Allow
your devices to sync the secrets securely, without storing them anywhere in the
cloud, providing maximum security.

## How it works

NOTE: Currently, **lockbox** can only create, store, and retrieve your secrets
locally. It is currently under heavy development to provide you with all the
features.

**lockbox** leverages the power and security of [OpenPGP](https://www.openpgp.org/)
to store your secrets on your hard drive securely. When running **lockbox** for
the first time an OpenPGP key will be generated for you.

The key consists of two parts, the public part and the private part. The public
part of the key is used only to encrypt your data and can not be used to decrypt
it. To decrypt the data that the public key part encrypted you need the private
part of the key to decrypt your secret back into its original form. Because of
this, your private key is further protected with a passphrase and can not be
used in any way unless you provide the passphrase.

### Generate password

To generate a new password use the **generate** subcommand:

```sh
lockbox generate [--no-symbols,-n] [--clip,-c] [--force,-f] password-name [length]
```

Generates a password of 25 length unless specified differently is created using
upper and lower case alphanumeric characters including symbols, unless the
`--no-symbols` flag is passed. If the `--clip` flag is used the generated
password is not printed to output, but rather copied to the clipboard. If a
password with that name already exists you are asked to confirm the overwrite
unless `--force` flag is used.

### Retrieving passwords

```sh
lockbox [--clip,-c] password-name
```

Prompts you for your private key passphrase, decrypts the stored password and
prints it to output, unless the `--clip` flag is used, then the password is
copied to the clipboard.

### Syncing your devices

Currently **lockbox** stores your passwords to your
`$HOME/.config/lockbox/data/` directory and does not provide a way for you to
automatically sync them with your other devices.

#### How we want it to work

Current state of **lockbox** is just a proof of concept and is nowhere near
completion. Missing features:

* lockbox server
    * intermediary server to help devices sync their data
    * optionally store users encrypted passwords
* Device synchronisation
    * optional storage of encrypted passwords on self-hosted lockbox server
* lockbox client
    * Password listing
    * Password renaming
    * Automatic clipboard clearing
    * Mobile client
    * Browser extensions
    * Optionally use `gpg-agent` for OpenPGP keys

## Installation & running

**lockbox** is comprised of two parts, a client and a daemon. The daemon and the
client applications. To install the current and latest in development versions:

```sh
go get gitlab.com/slax0rr/lockboxd
go get gitlab.com/slax0rr/lockbox
```

The daemon must always be running as it is doing all the heavy lifting and
should be run in the background:

```sh
lockboxd &
```

Currently this is a very manual process, but **lockbox** is in heavy
development and the process of running an using it will become easier and less
manually involved in the future.

### Binaries

See [Releases](https://gitlab.com/slax0rr/lockbox/-/releases) for latest binary
releases.

## Development

Should you wish to help with the development of **lockbox** please first read
the *Contributing* section bellow.

To start development first fork this repository and clone it to you local
machine. You will need the following tools for development:

* smartystreets/goconvey
* go-delve/delve
* protobuf/protoc-gen-go
* grpc
* vektra/mockery

All of the above can be installed by executing:

```sh
make install-convey \
    install-debug \
    install-mockery \
    install-protoc
```

When you are done and want to test your changes, simply execute `make`, it will
run the tests and build the executables and place them into the `dist/`
directory.

### Contributing

See [CONTRIBUTING.md](https://gitlab.com/slax0rr/lockbox/-/blob/master/CONTRIBUTING.md).
