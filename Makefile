PROJECT_ROOT=$(shell pwd)
DIST_DIR=dist/
CLIENT_CMD=lockbox
CLIENT_CMD_PATH=gitlab.com/slax0rr/lockbox/cmd/lockbox
DAEMON_CMD=lockboxd
DAEMON_CMD_PATH=gitlab.com/slax0rr/lockbox/cmd/lockboxd
SERVER_CMD=lockbox-server
SERVER_CMD_PATH=gitlab.com/slax0rr/lockbox/cmd/lockbox-server
GOCMD=go
DLV=dlv
PROTOC=protoc
MOCKERY=mockery
MOCK_PKG=mock
DEBUG_LISTEN=127.0.0.1:40000
CONVEY=${GOPATH}/bin/goconvey
CONVEY_PORT=8000
SWAGGER=swagger

SOURCES := $(shell find . -name "*.go" -not -path "./test/mock/*")

all: protoc gen-hub gen-client test client daemon hub

clean:
	rm dist/* || true
	rm -r test/mock/ || true
	rm internal/lockbox/pb/*.go || true
	rm -r internal/lockbox/apiclient || true
	rm -r cmd/lockbox-server || true
	find $(PROJECT_ROOT)/internal/lockbox/hub/restapi/* ! -name configure_lockbox.go -delete || true

client: dist/lockbox

daemon: dist/lockboxd

hub: dist/lockbox-server

dist/lockbox: $(SOURCES)
	${GOCMD} mod tidy
	${GOCMD} build -o ${DIST_DIR}${CLIENT_CMD} ${CLIENT_CMD_PATH}

dist/lockboxd: $(SOURCES)
	${GOCMD} mod tidy
	${GOCMD} build -o ${DIST_DIR}${DAEMON_CMD} ${DAEMON_CMD_PATH}

dist/lockbox-server: $(SOURCES)
	${GOCMD} mod tidy
	${GOCMD} build -o ${DIST_DIR}${SERVER_CMD} ${SERVER_CMD_PATH}

protoc: internal/lockbox/pb/lockbox.pb.go internal/lockbox/pb/lockbox_grpc.pb.go internal/lockbox/pb/lockbox_hub.pb.go internal/lockbox/pb/lockbox_hub_grpc.pb.go

internal/lockbox/pb/lockbox.pb.go: internal/lockbox/pb/lockbox.proto
	${PROTOC} --go_out=. \
		--go_opt=paths=source_relative \
		internal/lockbox/pb/lockbox.proto

internal/lockbox/pb/lockbox_grpc.pb.go: internal/lockbox/pb/lockbox.proto
	${PROTOC} --go-grpc_out=. \
		--go-grpc_opt=paths=source_relative \
		internal/lockbox/pb/lockbox.proto

internal/lockbox/pb/lockbox_hub.pb.go: internal/lockbox/pb/lockbox_hub.proto
	${PROTOC} --go_out=. \
		--go_opt=paths=source_relative \
		internal/lockbox/pb/lockbox_hub.proto

internal/lockbox/pb/lockbox_hub_grpc.pb.go: internal/lockbox/pb/lockbox_hub.proto
	${PROTOC} --go-grpc_out=. \
		--go-grpc_opt=paths=source_relative \
		internal/lockbox/pb/lockbox_hub.proto

gen-hub: cmd/lockbox-server/main.go

cmd/lockbox-server/main.go: internal/lockbox/hub/swagger.yaml
	${SWAGGER} generate server \
		-f ${PROJECT_ROOT}/internal/lockbox/hub/swagger.yaml \
		-t ${PROJECT_ROOT} \
		-P models.Principal \
		--server-package internal/lockbox/hub/restapi \
		--model-package internal/lockbox/models

gen-client: internal/lockbox/apiclient/lockbox_client.go

internal/lockbox/apiclient/lockbox_client.go: internal/lockbox/hub/swagger.yaml
	${SWAGGER} generate client \
		-f ${PROJECT_ROOT}/internal/lockbox/hub/swagger.yaml \
		-t ${PROJECT_ROOT}/internal/lockbox/ \
		-c apiclient

mocks: test/mock/LockboxClient.go test/mock/LockboxServer.go

test/mock/LockboxClient.go: internal/lockbox/pb/lockbox.pb.go internal/lockbox/pb/lockbox_grpc.pb.go
	${MOCKERY} \
		--dir=./internal/lockbox/pb \
		--name=LockboxClient \
		--outpkg=${MOCK_PKG} \
		--output=./test/mock/

test/mock/LockboxServer.go: internal/lockbox/pb/lockbox.pb.go internal/lockbox/pb/lockbox_grpc.pb.go
	${MOCKERY} \
		--dir=./internal/lockbox/pb \
		--name=LockboxServer \
		--outpkg=${MOCK_PKG} \
		--output=./test/mock/

debug: EXEC_CMD ?=
debug:
	${DLV} debug \
		--listen ${DEBUG_LISTEN} \
		--headless \
		${CLIENT_CMD_PATH} -- ${EXEC_CMD}

debug-hub:
	${DLV} debug \
		--listen ${DEBUG_LISTEN} \
		--headless \
		${SERVER_CMD_PATH}

test: mocks
	${GOCMD} test -v -failfast ./...

convey: mocks
	${CONVEY} -port ${CONVEY_PORT}

install-convey:
	GO111MODULE=off ${GOCMD} get -u github.com/smartystreets/goconvey

install-debug:
	GO111MODULE=off ${GOCMD} get -u github.com/go-delve/delve/cmd/dlv

install-protoc:
	GO111MODULE=off ${GOCMD} get -u google.golang.org/protobuf/cmd/protoc-gen-go
	GO111MODULE=off ${GOCMD} get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc

	echo "\n\nNOTE: Head to https://github.com/protocolbuffers/protobuf/blob/master/src/README.md to install the protoc executable.\n\n"

install-mockery:
	${GOCMD} get github.com/vektra/mockery/v2/.../

install-swagger:
	GO111MODULE=off ${GOCMD} get -u github.com/go-swagger/go-swagger

	cd ${GOPATH}/src/github.com/go-swagger/go-swagger ; go install ./cmd/swagger
