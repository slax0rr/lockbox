module gitlab.com/slax0rr/lockbox

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/ProtonMail/go-crypto v0.0.0-20210312170726-868f11d5e94a
	github.com/atotto/clipboard v0.1.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.27
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.1
	github.com/go-openapi/swag v0.19.15
	github.com/go-openapi/validate v0.20.1
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.1.2
	github.com/jessevdk/go-flags v1.5.0
	github.com/keybase/client v5.6.1+incompatible
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/pterm/pterm v0.12.6
	github.com/sirupsen/logrus v1.7.0
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4
	google.golang.org/genproto v0.0.0-20210121164019-fc48d45331c7 // indirect
	google.golang.org/grpc v1.35.0
	google.golang.org/protobuf v1.25.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.6
)
