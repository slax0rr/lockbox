package test

import (
	"fmt"
	"reflect"
)

// some stuff borrowed from https://github.com/smartystreets/assertions
// under its license https://github.com/smartystreets/assertions/blob/master/LICENSE.md

const (
	shouldHaveBeenEqual             = "Expected: '%v'\nActual:   '%v'\n(Should be equal)"
	shouldHaveBeenEqualTypeMismatch = "Expected: '%v' (%T)\nActual:   '%v' (%T)\n(Should be equal, type mismatch)"
)

func ShouldDeepEqual(actual interface{}, expected ...interface{}) string {
	for _, ex := range expected {
		if !reflect.DeepEqual(actual, ex) {
			message := "Not equal values"
			expectedSyntax := fmt.Sprintf("%v", ex)
			actualSyntax := fmt.Sprintf("%v", actual)
			if expectedSyntax == actualSyntax && reflect.TypeOf(expected) != reflect.TypeOf(actual) {
				message = fmt.Sprintf(shouldHaveBeenEqualTypeMismatch, expected, expected, actual, actual)
			} else {
				message = fmt.Sprintf(shouldHaveBeenEqual, expected, actual)
			}
			return message
		}
	}

	return ""
}
