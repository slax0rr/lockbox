package cmd

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
)

var (
	email      string
	password   string
	serverPort int64
	register   bool
)

var connectCmd = &cobra.Command{
	Use:   "connect [--email,-e] [--password,-w] [--port,-p] [-register,-r] hostname",
	Short: "Configure and establish a connection to a Lockbox server",
	Long: `To share secrets between different devices they must be connected to
the same server. With this command a new connection is configured and
automatically established with the Lockbox server.

If the optional "-r" flag is passed, a new account will be created on the server`,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("hostname must be specified")
		}

		opts := []cli.LockboxOpt{
			cli.WithServerHost(args[0]),
			cli.WithServerPort(serverPort),
			cli.WithServerEmail(email),
		}
		if password != "" {
			opts = append(opts, cli.WithServerPassword(password))
		}

		return cli.ConnectToServer(register, opts...)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		if password != "" {
			fmt.Println(`Using the "--password" flag is unsafe as it will be written to your shell history.
Consider omitting it, lockbox will ask you for it when needed.`)
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(connectCmd)

	connectCmd.PersistentFlags().StringVarP(&email, "email", "e", "",
		"username for authentication with the lockbox server")
	connectCmd.PersistentFlags().StringVarP(&password, "password", "w", "",
		"password for authentication with the lockbox server - UNSAFE")
	connectCmd.PersistentFlags().Int64VarP(&serverPort, "port", "p", 9378,
		"server port")
	connectCmd.PersistentFlags().BoolVarP(&register, "register", "r", false,
		"register a new account on the server")
}
