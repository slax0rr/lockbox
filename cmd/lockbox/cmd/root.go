package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
)

var cfgFile string
var debug, trace, clip bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "lockbox [--clip,-c] password-name",
	Short: "lockbox: manage your passwords",
	Long: `Lockbox stores your passwords securely on your device, leveraging
strong encryption algorithms.

Using the lockbox tool you can encrypt and decrypt your passwords, copy them to
your clipboard, push them to a remote server for sharing, and retrieve them from
it as well.`,
	DisableFlagsInUseLine: true,
	Args:                  cobra.ArbitraryArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			// list all passwords
			return nil
		}

		return cli.GetPassword(args[0], cli.WithClip(clip))
	},
	PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
		return cli.CloseGRPCConn()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(setLogLevel, initLockbox)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (default is ~/.config/lockbox/lockbox.toml)")
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "enable debug logging")
	rootCmd.PersistentFlags().BoolVar(&trace, "trace", false, "enable trace logging")
	rootCmd.Flags().BoolVarP(&clip, "clip", "c", false,
		"password will be copied to clipboard for 45 seconds")

}

func initLockbox() {
	if cli.Init(cfgFile) != nil {
		fmt.Println("init failed horribly")
		os.Exit(1)
	}
}

// setLogLevel sets the log level on the logrus logging utility.
func setLogLevel() {
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if trace {
		logrus.SetLevel(logrus.TraceLevel)
	}
}
