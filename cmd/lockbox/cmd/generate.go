package cmd

import (
	"strconv"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
)

var (
	generateNoSymbols bool
	generateClip      bool
	generateForce     bool
)

var generateCmd = &cobra.Command{
	Use:   "generate [--no-symbols,-n] [--clip,-c] [--force,-f] password-name [length]",
	Short: "Generate a new password, encrypt it, and add it to storage.",
	Long: `Generate a new password of the configured or specified length, using
alpha-numeric characters and symbols, unless the "--no-symbols" flag is used.
Password is encrypted and stored. Prompts before overwritting an existing
password, unless the "--force" flag is used. Generated password can optionally
be copied to the clipboard.

Name of the password can be only alpha-numeric characters along with a forward
slash ('/'), underscore ('_'), or dash ('-').`,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("a password name must be specified")
		}

		if len(args) > 1 {
			if _, err := strconv.Atoi(args[1]); err != nil {
				logrus.WithError(err).Debug("length parameter is not numeric")
				return errors.New("password lenght must be a number")
			}
		}

		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		opts := []cli.LockboxOpt{
			cli.WithNoSymbols(generateNoSymbols),
			cli.WithClip(generateClip),
			cli.WithForce(generateForce),
		}

		if len(args) > 1 {
			length, err := strconv.Atoi(args[1])
			if err != nil {
				logrus.WithError(err).
					Error("unable to parse specified length as numeric")
				return errors.New("password length must be a number")
			}
			opts = append(opts, cli.WithLength(length))
		}

		return cli.Generate(args[0], opts...)
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	generateCmd.PersistentFlags().BoolVarP(&generateNoSymbols, "no-symbols",
		"n", false, "password will contain only alpha-numeric characters")
	generateCmd.PersistentFlags().BoolVarP(&generateClip, "clip", "c", false,
		"password will be copied to clipboard for 45 seconds")
	generateCmd.PersistentFlags().BoolVarP(&generateForce, "force", "f", false,
		"force overwrite of an existing password")
}
