package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/cli"
)

var renewCmd = &cobra.Command{
	Use:   "renew",
	Short: "Renew devices key-pair",
	Long: `Generate a new key-pair for this device and re-encrypt all the
passwords with the new key-pair.

A generated key-pair will expire after a certain amount of time. When the key
does expire it can no longer be used to encrypt or decrypt passwords. Before
this happens you need to renew your key-pair in order to keep your access to
your passwords.`,
	Run: func(cmd *cobra.Command, args []string) {
		cli.RenewKeys()
	},
}

func init() {
	rootCmd.AddCommand(renewCmd)
}
