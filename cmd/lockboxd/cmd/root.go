package cmd

import (
	"fmt"
	"os"

	"github.com/keybase/client/go/spotty"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/slax0rr/lockbox/internal/lockbox/daemon"
)

var cfgFile string
var debug, trace, tty bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "lockboxd",
	Short: "lockboxd: lockbox daemon",
	Long: `Lockbox daemon doing the heavy lifting of encrypting, storing and
transmitting shared passwords.`,
	DisableFlagsInUseLine: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		defer daemon.Teardown(true)
		return daemon.Start()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(setLogLevel, initLockbox)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (default is ~/.config/lockbox/.lockboxd.toml)")
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "enable debug logging")
	rootCmd.PersistentFlags().BoolVar(&trace, "trace", false, "enable trace logging")
	rootCmd.PersistentFlags().BoolVarP(&tty, "tty", "t", false, "use current TTY for non-GUI pinentry")

}

func initLockbox() {
	ttyName := ""
	if tty {
		ttyName, err := spotty.Discover()
		if err != nil {
			panic(err)
		}
		if ttyName == "" {
			panic("unable to determine tty")
		}

	}

	if daemon.Init(cfgFile, ttyName) != nil {
		os.Exit(1)
	}
}

// setLogLevel sets the log level on the logrus logging utility.
func setLogLevel() {
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if trace {
		logrus.SetLevel(logrus.TraceLevel)
	}
}
