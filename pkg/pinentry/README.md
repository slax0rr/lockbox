Module copied under the BSD license from
[github.com/keybase/go/pinentry](https://github.com/keybase/client/tree/master/go/pinentry)
to allow usage of a standard [logrus](https://github.com/Sirupsen/logrus) logger
instance. Further edits were made to not require nearly the whole keybase client
codebase.

All credits go to their original authors.
