package event

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	registeredEvents map[string]*Event
)

func init() {
	registeredEvents = make(map[string]*Event)
}

type Message struct {
	Data []interface{}
}

type Handler func(*Message) error

type Event struct {
	handlers []Handler
}

func New() *Event {
	return &Event{
		handlers: make([]Handler, 0),
	}
}

func (ev *Event) RegisterHandler(h Handler) {
	ev.handlers = append(ev.handlers, h)
}

func Dispatch(name string, data ...interface{}) error {
	ev, ok := registeredEvents[name]
	if !ok {
		return fmt.Errorf("event '%s' does not exist", name)
	}

	msg := &Message{Data: data}

	for i := range ev.handlers {
		err := ev.handlers[i](msg)
		if err != nil {
			logrus.WithError(err).Error("event handler failed")
			return errors.Wrap(err, "event handler failed")
		}
	}
	return nil
}

func Create(name string) {
	registeredEvents[name] = New()
}

func Add(name string, ev *Event) {
	registeredEvents[name] = ev
}

func RegisterHandler(name string, h Handler) error {
	ev, ok := registeredEvents[name]
	if !ok {
		logrus.WithField("event", name).
			Error("can not register handler on non-existing event")
		return fmt.Errorf("event '%s' does not exist", name)
	}
	ev.RegisterHandler(h)
	return nil
}
