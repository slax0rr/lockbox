// TODO(slax0rr): properly set up requirements if a different pool option is
// used. If the custom pool is supplied, remove the requirements completely.
package pwdgen

import (
	"crypto/rand"
	"io"
	"math/big"

	"github.com/pkg/errors"
)

var (
	// alphaPool contains all the alphabetical characters for password
	// generation.
	alphaPool = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	// numPool contains all digit characters for password generation.
	numPool = []byte("0123456789")

	// symbolPool contains all the symbol characters that are valid for password
	// generation.
	symbolPool = []byte("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~")
)

func init() {
	defaultOpts.length = 24
	defaultOpts.pool = append(alphaPool, numPool...)
	defaultOpts.pool = append(defaultOpts.pool, symbolPool...)
	defaultOpts.reqs = []PasswordRequirement{
		ContainsLowerCaseChars(1),
		ContainsUpperCaseChars(1),
		ContainsNumbers(1),
		ContainsSymbols(1),
	}
}

// GeneratePassword by default generates 24 character long password using the
// alpha-numeric and symbols pool and writes it to the out writer. The password
// will guaranteed have at least one upper case character, one lower case
// character, one number, and one symbol.
//
// The defaults can be overriden using PasswordOpt options.
func GeneratePassword(out io.Writer, options ...PasswordOpt) error {
	opts := defaultOpts

	for _, opt := range options {
		opt(&opts)
	}

	plainText := make([]byte, opts.length)
	maxLen := big.NewInt(int64(len(opts.pool)))

	for i := 0; i < opts.length; i++ {
		p, err := rand.Int(rand.Reader, maxLen)
		if err != nil {
			return errors.Wrap(err, "unable to get random number")
		}

		plainText[i] = opts.pool[int(p.Int64())]
	}

	ok := true
	for _, req := range opts.reqs {
		if !req(plainText) {
			ok = false
			err := GeneratePassword(out, options...)
			if err != nil {
				return err
			}
			break
		}
	}

	if ok {
		n, err := out.Write(plainText)
		if err != nil {
			return err
		}

		if n != opts.length {
			return errors.New("could not write the whole password to the writer")
		}
	}

	return nil
}
