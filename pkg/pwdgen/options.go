package pwdgen

// passwordOptions for the generated password.
type passwordOptions struct {
	length int
	pool   []byte
	reqs   []PasswordRequirement
}

// defaultOpts holds the default options for password generation.
var defaultOpts = passwordOptions{}

// PasswordOpt function sets various password generation options.
type PasswordOpt func(*passwordOptions)

// WithPasswordLength defines the length of the generated password. Passing a
// lower than 6 value will cause a panic.
func WithPasswordLength(length int) PasswordOpt {
	// TODO(slax0rr): this check has no business here, handle it elesewhere and
	// without throwing a panic
	if length < 4 {
		panic("password length is too short")
	}

	return func(opts *passwordOptions) {
		opts.length = length
	}
}

// WithAlphaPool will use only alphabetical characters for password generation.
func WithAlphaPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = alphaPool
	}
}

// WithAlphanumPool will use the alphabetical and numerical characters for
// password generation.
func WithAlphanumPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = append(alphaPool, numPool...)
	}
}

// WithAlphaAndSymbolsPool will use the alphabetical and symbol characters for
// password generation.
func WithAlphaAndSymbolsPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = append(alphaPool, symbolPool...)
	}
}

// WithNumPool will use only digits for password generation.
func WithNumPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = numPool
	}
}

// WithNumAndSymbolsPool will use the digit and symbol characters for password
// generation.
func WithNumAndSymbolsPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = append(numPool, symbolPool...)
	}
}

// WithSymbolsPool will use only the symbol characters for password generation.
func WithSymbolsPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = symbolPool
	}
}

// WithAlphanumAndSymbolsPool will use the alphabetical characters, digits, and
// symbol characters for password generation.
func WithAlphanumAndSymbolsPool() PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = append(alphaPool, numPool...)
		opts.pool = append(opts.pool, symbolPool...)
	}
}

// WithCustomCharPool will use a custom pool of characters for password
// generation.
func WithCustomCharPool(pool []byte) PasswordOpt {
	return func(opts *passwordOptions) {
		opts.pool = pool
	}
}

func WithPasswordRequirements(reqs []PasswordRequirement) PasswordOpt {
	return func(opts *passwordOptions) {
		opts.reqs = reqs
	}
}
