package pwdgen_test

import (
	"bytes"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/slax0rr/lockbox/pkg/pwdgen"
)

func TestGeneratePassword(t *testing.T) {
	byteCheck := func(bs []byte, n int, ranges [][2]byte) bool {
		cnt := 0
	BS:
		for _, c := range bs {
			for _, r := range ranges {
				if c >= r[0] && c <= r[1] {
					cnt += 1
					continue BS
				}
			}

			if cnt >= n {
				return true
			}
		}

		return false
	}

	Convey("Given password generation options", t, func() {
		length := 10
		opts := []pwdgen.PasswordOpt{
			pwdgen.WithPasswordLength(length),
			pwdgen.WithAlphanumAndSymbolsPool(),
		}

		Convey("A generated password", func() {
			buf := new(bytes.Buffer)
			err := pwdgen.GeneratePassword(buf, opts...)
			So(err, ShouldBeNil)

			Convey("Is of expected length", func() {
				So(buf.Len(), ShouldEqual, length)
			})

			Convey("Contains at least one lower-case character", func() {
				So(byteCheck(buf.Bytes(), 1, [][2]byte{
					[2]byte{0x61, 0x7a},
				}), ShouldBeTrue)
			})

			Convey("Contains at least one upper-case character", func() {
				So(byteCheck(buf.Bytes(), 1, [][2]byte{
					[2]byte{0x41, 0x5a},
				}), ShouldBeTrue)
			})

			Convey("Contains at least one number character", func() {
				So(byteCheck(buf.Bytes(), 1, [][2]byte{
					[2]byte{0x30, 0x39},
				}), ShouldBeTrue)
			})

			Convey("Contains at least on symbol character", func() {
				So(byteCheck(buf.Bytes(), 1, [][2]byte{
					[2]byte{0x21, 0x2f},
					[2]byte{0x3a, 0x40},
					[2]byte{0x5b, 0x60},
					[2]byte{0x7b, 0x7e},
				}), ShouldBeTrue)
			})
		})
	})
}
