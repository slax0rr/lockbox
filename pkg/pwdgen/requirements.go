package pwdgen

// PasswordRequirement funcs are ran after the password is generated to check
// the password meets the requirements.
type PasswordRequirement func([]byte) bool

// containsBytesInRanges checks the byte slice if it contains N number of bytes
// in specified byte ranges.
func containsBytesInRanges(bs []byte, n int, ranges [][2]byte) bool {
	cnt := 0
BS:
	for _, c := range bs {
		for _, r := range ranges {
			if c >= r[0] && c <= r[1] {
				cnt += 1
				continue BS
			}
		}

		if cnt >= n {
			return true
		}
	}

	return false
}

// ContainsLowerCaseChars restricts the generated password to at least N number
// of lower case characters.
func ContainsLowerCaseChars(n int) PasswordRequirement {
	return func(password []byte) bool {
		return containsBytesInRanges(password, n, [][2]byte{[2]byte{0x61, 0x7a}})
	}
}

// ContainsUpperCaseChars restricts the generated password to at least N number
// of upper case characters.
func ContainsUpperCaseChars(n int) PasswordRequirement {
	return func(password []byte) bool {
		return containsBytesInRanges(password, n, [][2]byte{[2]byte{0x41, 0x5a}})
	}
}

// ContainsNumbers restricts the generated password to at least N number of
// digits.
func ContainsNumbers(n int) PasswordRequirement {
	return func(password []byte) bool {
		return containsBytesInRanges(password, n, [][2]byte{[2]byte{0x30, 0x39}})
	}
}

// ContainsSymbols restricts the generated password to at least N number of
// symbols.
func ContainsSymbols(n int) PasswordRequirement {
	return func(password []byte) bool {
		return containsBytesInRanges(password, n, [][2]byte{
			[2]byte{0x21, 0x2f},
			[2]byte{0x3a, 0x40},
			[2]byte{0x5b, 0x60},
			[2]byte{0x7b, 0x7e},
		})
	}
}
