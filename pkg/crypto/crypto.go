package crypto

import (
	"errors"
	"io"
	"io/ioutil"

	"github.com/ProtonMail/go-crypto/openpgp"
	"github.com/ProtonMail/go-crypto/openpgp/armor"
)

const MessageType = "PGP MESSAGE"

// Encrypt reads the plaintext Reader, encrypts it using the public keys of the
// packages entity list, and writes the encrypted data to the ciphertext writer.
func Encrypt(plaintext io.Reader, ciphertext io.Writer) error {
	return encrypt(append(entityList, primaryKey.Entity), plaintext,
		ciphertext)
}

// EncryptWithKey reads the plaintext Reader, encrypts it using the public key
// part of the provded key, and writes the encrypted data to the ciphertext
// writer.
func EncryptWithKey(key *Key, plaintext io.Reader, ciphertext io.Writer) error {
	return encrypt(openpgp.EntityList{key.Entity}, plaintext, ciphertext)
}

func encrypt(el openpgp.EntityList, plaintext io.Reader, ciphertext io.Writer) error {
	w, err := openpgp.Encrypt(ciphertext, el, nil, nil, config.Config)
	if err != nil {
		return err
	}

	msg, err := ioutil.ReadAll(plaintext)
	if err != nil {
		return err
	}

	_, err = w.Write(msg)
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	return nil
}

// Decrypt the ciphertext Reader data stream into the plaintext Writer using
// the packages primary keys private key combined with the packages entity list
// should it contain any decryption keys.
//
// If no decryption keys are found after the entity list and primary key are
// combined an error is returned.
func Decrypt(ciphertext io.Reader, plaintext io.Writer) error {
	if primaryKey.cancelEncrypt != nil {
		primaryKey.cancelEncrypt.Reset(config.UnlockDuration)
	}
	return decrypt(append(entityList, primaryKey.Entity), ciphertext,
		plaintext)
}

// DecryptWithKey the ciphertext Reader data stream into the plaintext Writer
// using the provided keys private key.
//
// If the provided key does not contain a valid decryption key an error is
// returned.
func DecryptWithKey(key *Key, ciphertext io.Reader, plaintext io.Writer) error {
	return decrypt(openpgp.EntityList{key.Entity}, ciphertext, plaintext)
}

func decrypt(el openpgp.EntityList, ciphertext io.Reader, plaintext io.Writer) error {
	if len(el.DecryptionKeys()) == 0 {
		return errors.New("no decryption key found")
	}

	md, err := openpgp.ReadMessage(ciphertext, el, nil, nil)
	if err != nil {
		return err
	}

	bs, err := ioutil.ReadAll(md.UnverifiedBody)
	if err != nil {
		return err
	}

	n, err := plaintext.Write(bs)
	if err != nil {
		return err
	}

	if n != len(bs) {
		return errors.New("unable to write the whole decrypted message to plaintext")
	}

	return nil
}

// ArmorEncode encodes the received data in the armor format and writes it to
// the armored Writer.
func ArmorEncode(data io.Reader, armored io.Writer) error {
	w, err := armor.Encode(armored, MessageType, nil)
	if err != nil {
		return err
	}

	msg, err := ioutil.ReadAll(data)
	if err != nil {
		return err
	}

	_, err = w.Write(msg)
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	return nil
}

// ArmorDecode decodes the data from the armored Reader and writes it to the
// decoded Writer. If less data is written to the Writer than the lenght of the
// decoded data an error is returned.
func ArmorDecode(armored io.Reader, decoded io.Writer) error {
	block, err := armor.Decode(armored)
	if err != nil {
		return err
	}

	bs, err := ioutil.ReadAll(block.Body)
	if err != nil {
		return err
	}

	n, err := decoded.Write(bs)
	if err != nil {
		return err
	}

	if len(bs) != n {
		return errors.New("did not write decoded data completely")
	}

	return nil
}
