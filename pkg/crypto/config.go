package crypto

import (
	"crypto"
	"time"

	"github.com/ProtonMail/go-crypto/openpgp/packet"
)

type Config struct {
	// KeyExpiry defines the expiration of a OpenPGP private key. After that
	// the key is no longer valid and unusable.
	KeyExpiry time.Duration

	// UnlockDuration defines how long a private key may be kept decrypted
	// before it is automatically encrypted and requires its passphrase to be
	// decrypted again.
	UnlockDuration time.Duration

	// Config is the "openpgp/packet".Config struct
	Config *packet.Config
}

var config *Config = &Config{
	KeyExpiry:      2 * 365 * 24 * 60 * 60 * time.Second,
	UnlockDuration: 5 * time.Minute,
	Config: &packet.Config{
		RSABits:       4096,
		DefaultHash:   crypto.SHA256,
		DefaultCipher: packet.CipherAES256,
	},
}

func SetRSABits(bits int) {
	config.Config.RSABits = bits
}

func SetKeyExpiry(expiry time.Duration) {
	config.KeyExpiry = expiry
}

func SetUnlockDuration(dur time.Duration) {
	config.UnlockDuration = dur
}

func SetPacketConfig(cfg *packet.Config) {
	config.Config = cfg
}
