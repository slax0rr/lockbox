package crypto

import (
	"bytes"
	"io"
	"time"

	"github.com/ProtonMail/go-crypto/openpgp"
	"github.com/ProtonMail/go-crypto/openpgp/armor"
	"github.com/ProtonMail/go-crypto/openpgp/packet"
	"github.com/pkg/errors"
)

var (
	entityList openpgp.EntityList
	primaryKey *Key
)

// Values from https://tools.ietf.org/html/rfc4880#section-9
const (
	md5       = 1
	sha1      = 2
	ripemd160 = 3
	sha256    = 8
	sha384    = 9
	sha512    = 10
	sha224    = 11
)

func init() {
	entityList = make(openpgp.EntityList, 0)
	primaryKey = new(Key)
}

// SetKeyAsPrimary sets a custom key as the primary key used for encrypting and
// decrypting.
func SetKeyAsPrimary(k *Key) {
	primaryKey = k
}

// DecodeKey reads the armored public key, decodes it, and returns it as the Key
// obejct.
func DecodeKey(r io.Reader) (*Key, error) {
	armorBlock, err := armor.Decode(r)
	if err != nil {
		return nil, errors.Wrap(err, "key armor encoding invalid")
	}

	rdr := packet.NewReader(armorBlock.Body)
	entity, err := openpgp.ReadEntity(rdr)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read key entity")
	}

	key := new(Key)
	key.Entity = entity

	return key, nil
}

// LoadKey reads the armored key, decodes it, and sets it as the primary key.
// If the key is encrypted it is not decrypted.
func LoadKey(r io.Reader) (err error) {
	key, err := DecodeKey(r)
	if err != nil {
		return err
	}

	primaryKey.Entity = key.Entity

	return nil
}

// PrivateKeyEncrypted returns the encryption state of the private key of the
// primary key.
func PrivateKeyEncrypted() bool {
	if primaryKey.Encrypted() {
		return true
	}

	primaryKey.cancelEncrypt.Reset(config.UnlockDuration)
	return false
}

// DecryptPrivateKey decrypts the private key and starts a cancelation period
// after which the key will be re-encrypted again. The period can be controlled
// by the `UnlockDuration` parameter of config.
func DecryptPrivateKey(passphrase []byte) error {
	primaryKey.cancelEncrypt = time.AfterFunc(config.UnlockDuration, func() {
		if err := primaryKey.Encrypt(passphrase); err != nil {
			panic(err)
		}
	})
	return primaryKey.Decrypt(passphrase)
}

// GenerateKeyPair generates a new public and private key pair and returns it.
// The private key part is not encrypted.
func GenerateKeyPair(name, email string) (key *Key, err error) {
	key = new(Key)

	key.Entity, err = openpgp.NewEntity(name, "lockbox key", email,
		config.Config)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new entity")
	}

	// Set expiry and algorithms. Self-sign the identity.
	dur := uint32(config.KeyExpiry.Seconds())
	for _, id := range key.Entity.Identities {
		id.SelfSignature.KeyLifetimeSecs = &dur

		id.SelfSignature.PreferredSymmetric = []uint8{
			uint8(packet.CipherAES256),
			uint8(packet.CipherAES192),
			uint8(packet.CipherAES128),
			uint8(packet.CipherCAST5),
			uint8(packet.Cipher3DES),
		}

		id.SelfSignature.PreferredHash = []uint8{
			sha256,
			sha1,
			sha384,
			sha512,
			sha224,
		}

		id.SelfSignature.PreferredCompression = []uint8{
			uint8(packet.CompressionZLIB),
			uint8(packet.CompressionZIP),
		}

		err := id.SelfSignature.SignUserId(id.UserId.Id,
			key.Entity.PrimaryKey, key.Entity.PrivateKey, config.Config)
		if err != nil {
			return nil, errors.Wrap(err, "unable to self sign")
		}
	}

	// Self-sign the Subkeys
	for _, subkey := range key.Entity.Subkeys {
		subkey.Sig.KeyLifetimeSecs = &dur
		err := subkey.Sig.SignKey(subkey.PublicKey, key.Entity.PrivateKey,
			config.Config)
		if err != nil {
			return nil, errors.Wrap(err, "unable to sign sub-key")
		}
	}

	return
}

// GeneratePrimaryKey generates a new public and private key pair, encrypts the
// private key part and sets the pair as the primary key.
func GeneratePrimaryKey(name, email, passphrase string) (keyID uint64, err error) {
	primaryKey, err = GenerateKeyPair(name, email)
	if err != nil {
		return 0, err
	}

	err = primaryKey.Encrypt([]byte(passphrase))
	if err != nil {
		return 0, err
	}

	return primaryKey.Entity.PrimaryKey.KeyId, nil
}

// ArmorPublicKey armors the public key part of the primary key and returns it.
func ArmorPublicKey() ([]byte, error) {
	return primaryKey.Armor()
}

// ArmorPrivateKey armors the private key part of the primary key and returns
// it.
func ArmorPrivateKey() ([]byte, error) {
	return primaryKey.ArmorPrivate()
}

const PrivateKeyMessageType = "LOCKBOX PRIVATE KEY"

type Key struct {
	Entity        *openpgp.Entity
	cancelEncrypt *time.Timer
}

func (k *Key) Encrypted() bool {
	return k.Entity.PrivateKey.Encrypted
}

// Encrypt the private key.
func (k *Key) Encrypt(passphrase []byte) error {
	err := k.Entity.PrivateKey.Encrypt(passphrase)
	return errors.Wrap(err, "could not encrypt private key")
}

// Decrypt the private key.
func (k *Key) Decrypt(passphrase []byte) error {
	err := k.Entity.PrivateKey.Decrypt(passphrase)
	return errors.Wrap(err, "could not decrypt private key")
}

// Armor encodes the public key part.
func (k Key) Armor() ([]byte, error) {
	pubKey := new(bytes.Buffer)
	err := k.Entity.Serialize(pubKey)
	if err != nil {
		return nil, err
	}

	out := new(bytes.Buffer)
	w, err := armor.Encode(out, openpgp.PublicKeyType, nil)
	if err != nil {
		return nil, err
	}

	_, err = w.Write(pubKey.Bytes())
	if err != nil {
		return nil, err
	}

	err = w.Close()
	if err != nil {
		return nil, err
	}

	return out.Bytes(), nil
}

// ArmorPrivate armor encodes the private key part. If the key is currently
// decrypted an error is returned.
func (k Key) ArmorPrivate() ([]byte, error) {
	if !k.Entity.PrivateKey.Encrypted {
		return nil, errors.New("unable to armor private key as key not encrypted")
	}

	out := new(bytes.Buffer)
	w, err := armor.Encode(out, openpgp.PrivateKeyType, nil)
	if err != nil {
		return nil, errors.Wrap(err, "unable to init armor encoder")
	}

	serialized := new(bytes.Buffer)
	err = k.Entity.SerializePrivateWithoutSigning(serialized, config.Config)
	if err != nil {
		return nil, errors.Wrap(err, "could not serialize private key")
	}

	_, err = w.Write(serialized.Bytes())
	if err != nil {
		return nil, errors.Wrap(err, "failed to write private key to armor encoder")
	}

	err = w.Close()
	if err != nil {
		return nil, errors.Wrap(err, "armor encoding failed")
	}

	return out.Bytes(), nil
}
