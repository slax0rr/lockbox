# Contributing to lockbox

**lockbox** is an open source project. Anyone can contribute to it and we
appreciate your help!

## Contributing code

Currently the only way to contribute to **lockbox** is by contributing code. To
do so, please head over to [Issues](https://gitlab.com/slax0rr/lockbox/-/issues),
pick an issue and implement it.

To contribute, fork the repository, implement the issue on your forked
repository and open a Merge Request to the *master* branch. Your code must be
`go fmt` formatted and must pass the `go vet` examination. Further styling rules
include a soft line length limit at 80 and a hard limit at 120 characters.
